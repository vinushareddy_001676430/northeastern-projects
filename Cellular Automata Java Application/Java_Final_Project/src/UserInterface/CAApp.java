/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Vinusha
 */
public class CAApp implements ActionListener{

    
    protected static JFrame frame ;
    protected JPanel Panel = null;
    protected JLabel label=null;
    protected JComboBox combobox=null;
    protected JTextField txtField=null;    
    protected JButton button_0 = null;
    protected JButton button_1 = null;
    protected JPanel panel1=null;
    protected JLabel ruleLabel=null;
   

    

	public  CAApp() {
		initGUI();
		
	}
	
	@Override
    public void actionPerformed(ActionEvent e) {
     
    }
    
	
	private void initGUI() {
		frame = new JFrame();
		frame.setTitle("CAApp");
		frame.setSize(1000, 1000);
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		frame.setLayout(new BorderLayout());
               // frame.add(getPanel1());
		frame.add(getPanel(),BorderLayout.NORTH);
                
		frame.setVisible(true);
	   }

	public JPanel getPanel() {
		Panel = new JPanel();
		Panel.setLayout(new FlowLayout());
                
		button_0 = new JButton("Start");
		button_0.addActionListener(this);
		Panel.add(button_0);
		
		button_1 = new JButton("Stop");
		button_1.addActionListener(this);
		Panel.add(button_1);
                
                label=new JLabel("Enter number of Generations: ");
		txtField=new JTextField("EnternumberofGenerations");
                 Panel.add(label);
		Panel.add(txtField);
               
		ruleLabel=new JLabel("Select a Rule");
                Panel.add(ruleLabel);
		combobox=new JComboBox();
		combobox.addItem("rule 90");              
		combobox.addItem("rule 122");
                combobox.addItem("rule 165");
		combobox.addItem("rule 201");	
                combobox.addItem("rule 188");
		combobox.addItem("rule 196");
                combobox.addItem("rule 206");
		combobox.addItem("rule 225");
		combobox.addItem("rule 231");
             			
		
		Panel.add(combobox);     
		
		return Panel;
	}
     
}
