package UserInterface;



import Algorithm.CAGeneration;
import Algorithm.CAGenerationSet;
import Algorithm.CARule;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyApp {
		
	
	
	
	public static void main(String args[])
	{
            
		System.out.println(Thread.currentThread().toString());
		Run run=new Run();
                
	}


	
}

	class  Run extends CAApp {
	
	private int generations;
	private String ruleSelected;
	private boolean flag;
	
	
	
	
	public Run()
	{
            
	frame.setTitle("MyApp");
		
	}	
	
	//actions for start and stop
	public void actionPerformed(ActionEvent arg0) 
	{
	
	 if(arg0.getSource()==button_0)
		{
                    System.out.println("Start button is pressed");
			
			
		//Validation of Generations input
			
                try { 
		        Integer.parseInt(txtField.getText()); 
		    } catch(NumberFormatException e) { 
		        JOptionPane.showMessageDialog(null, "Please enter numbers only");
		    }
			
			if(txtField.getText().isEmpty() || Integer.parseInt(txtField.getText())==0)
			{
				JOptionPane.showMessageDialog(null, "Invalid Number of generations");
				return;
			}
			
			//Taking the inputs from the UI
                        int gen=Integer.parseInt(txtField.getText());
			String comboselected1=combobox.getSelectedItem().toString();
			this.generations=gen;
			this.ruleSelected=comboselected1;			
			
			
			System.out.println(Thread.currentThread().toString());
			flag=true;	
			CAGenerationSet.SetNull();
			CARule.setNull();
			
                        System.out.println("Selected Rule is: "+ruleSelected);
			//Generate the number of generations desired with specific rules printing each generation
			for(int i=0;i<generations;i++)
			{
			CAGenerationSet cags=CAGenerationSet.getInstance();
			cags.GenerateGenSet(ruleSelected);
                          
			}                             
			
			//add the canvas to the frame       

                            
                        CanvasPanel m=new CanvasPanel(generations);
                        m.setMaximumSize(new Dimension(1000, 1000));
                        m.setLayout(new FlowLayout(FlowLayout.CENTER));
                        frame.getContentPane().add(m);
                        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        frame.setVisible(true);

		}
		else if (arg0.getSource()==button_1){
			
			flag=false;
			System.out.println("Stop button is pressed");
			//setting the flag to false
                        CanvasPanel.setBool(flag);
			
                }
        }
	  
       }

//inner class used to paint the canvas	
class CanvasPanel extends JPanel {
	
	
	private int index,count;
	private static int counter;
	private static boolean run;
	private int generations;
	

	
    CanvasPanel(int generations) {
    	
    	//intilaizing the r (no of generations)
    	run=true;
    	this.generations=generations;
    	count=1;
    	
    	
    	
    }
    
    //method to set the boolean value
    public static void setBool(boolean bool)
    {
    	run=bool;
    	System.out.println("Bool Value: "+run);
    }
    
    public boolean getBool()
    {
    	return run;
    }
    
    
    
    //paintComponent method is automatically called by AWT thread
    
    public void paint(Graphics g1) {
    	
    	//this method resets the canvas
    	super.paint(g1);
    	drawCellularAutomata(g1);
    	
    }
    
    
    	
    	
    public void drawCellularAutomata(Graphics g1)
    	{
    		
    		Graphics2D g2=(Graphics2D) g1;
    		
    		for(int l=0;l<count;l++)
                    
    		{
    			
	    		CAGeneration cag=CAGenerationSet.getCags().get(l);
	    		System.out.println(run);
	    		
	        	index=counter;
	        	counter=counter+5;
    		
	            for(int i=0;i<cag.getCag().size();i++)
		        {
	            	
		        	if(cag.getCag().get(i).getCellvalue()==0)
		        	{
		        		
		        		g2.setColor(Color.BLACK);		        		
		        		g2.fillRect(8*i,index,8,8);
		        		
		        		
		        		
		        	}
		        	else
		        	{
		        		g2.setColor(Color.RED);
		        		g2.fillRect(8*i,index,8,8);
		
		        	}
		         
		        }   
    		}
    	  
                counter=0;
    	
    	
    	 if(count<CAGenerationSet.getCags().size() && run==true)
         {
    		 try {
                     Thread.sleep(1000);
        		} catch (InterruptedException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
    		 repaint(); //repaints the canvas with each generation 
    		 count++;
         }    		
    	 
    	
    }
    
      
}//end of inner class CanvasPanel
