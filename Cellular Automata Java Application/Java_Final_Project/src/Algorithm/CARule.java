package Algorithm;


public class CARule {
	
	
	private static CARule carule;
        private int[] cells;
	private static int[] ruleval;
	
	
	private CARule()
	 {
		cells=new int[65];
		
		ruleval= new int[]{0,1,0,1,1,0,1,0};
                
		//Base Values
		for(int i=0;i<cells.length;i++)
		{
			cells[i]=0;
		}
		cells[cells.length/2]=1;
		
		
		//adding, displaying base values 
		CAGeneration cag=new CAGeneration();
		cag.printGeneration(cells);
		CAGenerationSet.getCags().add(cag);
		 
	 }
	
	
	public void CARule1(String comboselected)
	{
	
            //Method: To override the ruleval 
        if(comboselected.equals("rule 90"))
        {
            ruleval= new int[]{0,1,0,1,1,0,1,0};
        }
       
        else if(comboselected.equals("rule 122"))
        {
            
            ruleval= new int[]{0,1,1,1,1,0,0,1};
            
        }
        else if(comboselected.equals("rule 201"))
        {
        	
            ruleval= new int[]{1,1,0,0,1,0,0,1};
        }
		 
        else if(comboselected.equals("rule 231"))
        {
        	
            ruleval= new int[]{1,1,1,0,0,1,1,1};
        }
		 
        else if(comboselected.equals("rule 188"))
        {
        	
            ruleval= new int[]{1,0,1,1,1,1,0,0};
        }
		 
        else if(comboselected.equals("rule 196"))
        {
        	
            ruleval= new int[]{1,1,0,0,0,1,0,0};
        }
		 
        else if(comboselected.equals("rule 206"))
        {
        	
            ruleval= new int[]{1,1,0,0,1,1,1,0};
        }
		 
        else if(comboselected.equals("rule 225"))
        {
        	
            ruleval= new int[]{1,1,1,0,0,0,0,1};
        }
        
        else if(comboselected.equals("rule 165"))
        {
        	
            ruleval= new int[]{1,0,1,0,0,1,0,1};
        }
	}
	
	public static CARule getInstance()
	{
		if(carule==null)
		{
			carule=new CARule();
			
		}
		return carule;
	}
	 public int[] CellNextGeneration()
	 {
           
                 int[] cellnextgen=new int[cells.length];
		 for(int i=1;i<cells.length-1;i++)
		 {
			 int left=cells[i-1];
			 int mid=cells[i];
			 int right=cells[i+1];
			 cellnextgen[i]=rules(left,mid,right);
			 
		 }
		 cells=cellnextgen;
		 
		return cells;
		 
		
	 }
	 
	 public int rules(int a,int b,int c)
	 {
		 String s= "" + a + b + c;
		 int index=Integer.parseInt(s,2);
		 return ruleval[index];
		 
	 }
	 
	 public static void setNull()
	 {
		 
		 carule=null;
	 }

}
