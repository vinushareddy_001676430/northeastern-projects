/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algorithm;

import java.util.ArrayList;

/**
 *
 * @author Vinusha
 */
public class CAGenerationSet {
    private static CAGenerationSet cags;
    private static ArrayList<CAGeneration> genset;
	
	private CAGenerationSet()
	{
		genset=new ArrayList<>();
		
		
	}
	
	
	
	public void GenerateGenSet(String comboselected)
	{
           
		CARule carule=CARule.getInstance();
		carule.CARule1(comboselected); //sets the ruleset according to the selected rule
                
		int[] nextgen=carule.CellNextGeneration();		
		CAGeneration cag=new CAGeneration();
		
		cag.printGeneration(nextgen);
		genset.add(cag);     
        }
        
        
         public static ArrayList<CAGeneration> getCags() {
	
		return genset;
	}
	
	public static void SetNull()
	
        {
		cags=null;
		
		
	}
	
	public static CAGenerationSet getInstance()
	{
		if(cags==null)
		{
			cags=new CAGenerationSet();
		}
		return cags;
	}
	



}
