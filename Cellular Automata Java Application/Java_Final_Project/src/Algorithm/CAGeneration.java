package Algorithm;


import java.util.ArrayList;


public class CAGeneration {
	
	private ArrayList<CACell> cag;
	
	public CAGeneration()
	{
		cag=new ArrayList<>();
		
	}

	public ArrayList<CACell> getCag() {
		return cag;
	}

	public void setCag(ArrayList<CACell> cag) {
		this.cag = cag;
	}
	
		
	public void printGeneration(int[] cell)
	{
		
		for(int i=0;i<cell.length;i++)
		{
			CACell a= new CACell();
			a.setCellvalue(cell[i]);
			cag.add(a);
		}
		
		//printing each generation
		System.out.print("\n");
		System.out.print("*");
		for(int i=0;i<cag.size();i++)
		{
                   
                    if(cag.get(i).getCellvalue()==0)
                    {
                        System.out.print(" ");
                    }
                    else{
                        System.out.print(cag.get(i).getCellvalue());
                    }
		}
                 System.out.print("*");
                 System.out.print("\n");
                 
                 
	}
	
}
