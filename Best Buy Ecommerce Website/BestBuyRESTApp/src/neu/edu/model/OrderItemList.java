package neu.edu.model;

import java.util.ArrayList;
import java.util.List;

import neu.edu.controller.data.OrderItemResponse;
import neu.edu.mapping.Orderitem;



public class OrderItemList {

	private List<OrderItemResponse> ois = new ArrayList<>();

	public List<OrderItemResponse> getOis() {
		return ois;
	}

	public void setOis(List<OrderItemResponse> ois) {
		this.ois = ois;
	}
	
	
	
}
