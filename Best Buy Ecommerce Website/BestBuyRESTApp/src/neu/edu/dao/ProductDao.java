package neu.edu.dao;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;
import neu.edu.mapping.Seller;


@Service
public class ProductDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	

	@Transactional
	public boolean createProduct(Product p,int sellerId){
	Session session = sessionFactory.getCurrentSession();

		session.persist(p);
				
		return true;
		
	}

	public List<Product> listProducts() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Product");
		

		List<Product> prod = (List<Product>) query.list();
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return prod;
	}
	
	public List<Object[]> listpopularProducts() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query query = session.createSQLQuery("Select  p.ProductID,p.ProductName,p.Description,p.Price from product p"
					+ "  inner join orderitem oi on p.ProductID = oi.ProductID"
					+ "  inner join bestbuy.order o on oi.OrderID = o.OrderID"
					+ "  group by p.ProductID"
					+ "  order by count(o.OrderID) Desc");
		
		String sql="Select  p.ProductID,p.ProductName,p.Description,p.Price,count(o.OrderID) from product p"
				+ " inner join orderitem oi on p.ProductID = oi.ProductID"
				+ " inner join bestbuy.order o on oi.OrderID = o.OrderID"
				+ " group by p.ProductID"
				+ " order by count(o.OrderID) Desc";
		
		System.out.println("Sql query for popular products relevance: "+sql);
		
		List<Object[]> prod = (List<Object[]>) query.list();
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return prod;
	}
	
	public List<Object[]> listOis(int sellid){
		
		Session session = sessionFactory.openSession();
		
		String sql = "select o.OrderItemID,o.Quantity,o.OrderID,p.ProductName,o.Order_Status from seller s " 
				+"inner join product p on s.SellerID=p.SellerID  "
				+"inner join orderitem o on p.ProductID=o.ProductID "
				+ "where o.OrderID IS NOT NULL and o.Order_Status is null and s.SellerID=:sellid";
		
		System.out.println("query to fetch order items for seller to mark "+sql);
		Query query = session.createSQLQuery(sql);
		query.setInteger("sellid", sellid);
		
	//	List<Orderitem> listoi=new List<Orderitem>();
		
		
		List<Object[]> prod =(List<Object[]>) query.list();
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return prod;
	}
	
}
