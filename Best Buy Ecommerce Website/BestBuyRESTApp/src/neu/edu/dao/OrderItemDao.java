package neu.edu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.controller.data.CardUpdaterequest;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;
import neu.edu.mapping.User;

@Service
public class OrderItemDao {

	

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	@Transactional
	public boolean createOi(Orderitem oi,int custid){
	Session session = sessionFactory.getCurrentSession();
				
//	if(p.getSeller().getSellerId()==sellerId){
//		
//	}
	
	Query query = session.createQuery("from Customer where customerId=:custid");
	
	query.setString("custid", String.valueOf(custid));
	Customer cust = (Customer) query.uniqueResult();
	
	//cust.getCustomerId();	
	oi.setCustomer(cust);
	session.persist(oi);
					
		
		return true;
		
	}
	
	public List<Object[]> listOrderItems(String custid) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query query = session.createSQLQuery("select * from orderitem where CustomerID=:custid and OrderID is NULL");
		query.setString("custid", custid);

		List<Object[]> oi = (List<Object[]>) query.list();
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return oi;
	}
	
	@Transactional
	public boolean mark(String oid) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();	
		Query query = session.createQuery("from Orderitem where orderItemId=:oid");
		query.setInteger("oid", Integer.parseInt(oid));
		
		Orderitem oi=(Orderitem)query.uniqueResult();
		
		oi.setOrderStatus("Shipped");		
		session.update(oi);
		
		try {
			
		} catch (Exception ex) {
			return false;
		}
	return true;
	}
	
	
}
