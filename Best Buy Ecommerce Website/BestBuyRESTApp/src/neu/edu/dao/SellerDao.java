package neu.edu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.mapping.Seller;

@Service
public class SellerDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	
public List<Seller> listSellers(){
		
		Session session = sessionFactory.openSession();
		

		Query query = session.createQuery("from Seller");		
	
		List<Seller> sell =(List<Seller>) query.list();
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return sell;
	}
}
