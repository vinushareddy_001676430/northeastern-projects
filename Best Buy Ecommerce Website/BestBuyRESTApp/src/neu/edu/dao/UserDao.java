package neu.edu.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.eclipse.jdt.internal.compiler.flow.FinallyFlowContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.controller.data.RegistrationRequest;
import neu.edu.controller.data.UserSession;
import neu.edu.controller.data.md5;
import neu.edu.mapping.Admin;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Seller;
import neu.edu.mapping.User;
import neu.edu.service.RegistrationService;




@Service
public class UserDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public boolean registrationCustomer1(User user,Customer cust){
		Session session = sessionFactory.getCurrentSession();
		
		session.persist(user);
		session.persist(cust);
		
		return true;
		
	}
	
	@Transactional
	public boolean registrationSeller(User user,Seller seller){
		Session session = sessionFactory.getCurrentSession();
					session.persist(user);
					session.persist(seller);
		
		return true;
		
	}
	
	@Transactional
	public UserSession validateUser(String username, String password) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		
			
			Query query = session.createQuery("from User "
					+ "where userName=:un ");
					
		query.setString("un", username);
		//query.setString("pass", password);
	
		

		User user = (User) query.uniqueResult();
		
		
		UserSession userSession = null;
		userSession=new UserSession();
		
		if(md5.md5(password).equals(user.getPassword())){
		
		
		if(user.getRole().equals("customer")){
			
		
			
			Set<Customer> custset=user.getCustomers();
			Iterator iterator=custset.iterator();
			
			while(iterator.hasNext()){
				Customer cust=(Customer)iterator.next();
				int custid=cust.getCustomerId();
				userSession.setId(custid);
				userSession.setName(user.getFirstName());
				userSession.setRole(user.getRole());
				userSession.setSuccess(true);
			}
			
		}
		
		
		if(user.getRole().equals("seller")){
		
			Set<Seller> sellset=user.getSellers();
			Iterator iterator=sellset.iterator();
			
			while(iterator.hasNext()){
				Seller sell=(Seller)iterator.next();
				if(sell.getStatus().equals("Enable")){
					
			
				int sellid=sell.getSellerId();
				userSession.setId(sellid);
				userSession.setName(user.getFirstName());
				userSession.setRole(user.getRole());
				userSession.setSuccess(true);
				}
				
				else{
					userSession.setMessage("Account Deactivated");
				}
				
			}
			
		}
		}
		
		if(user.getRole().equals("admin")){
			
		
			
			Set<Admin> set=user.getAdmins();
			Iterator iterator=set.iterator();
			
			while(iterator.hasNext()){
				Admin admin=(Admin)iterator.next();
				int adm=admin.getAdminId();
				userSession.setId(adm);
				userSession.setName(user.getFirstName());
				userSession.setRole(user.getRole());
				userSession.setSuccess(true);
			}
			
		}
		
		
		return userSession;
	}


	
	}
