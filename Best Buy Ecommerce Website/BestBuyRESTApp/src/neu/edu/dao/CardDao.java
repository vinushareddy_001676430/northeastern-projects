package neu.edu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.controller.data.CardUpdaterequest;
import neu.edu.mapping.Customer;
import neu.edu.mapping.User;

@Service
public class CardDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public Object[] listcarddetails(String custid) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query query = session.createSQLQuery("select * from customer where CustomerID=:custid");
		query.setString("custid", custid);
		System.out.println("in dao");
		
		Object[] custs = (Object[]) query.uniqueResult();
				
		return custs;
	}
	
@Transactional
	public boolean updatecard(CardUpdaterequest cr,String uname,String custid) {
		// TODO Auto-generated method stub
		
		Session session = sessionFactory.getCurrentSession();		 
		
	

		
		Customer cust=new Customer();
		cust.setCustomerId(Integer.parseInt(custid));
		User u=new User();
		u.setUserName(uname);
		
		cust.setUser(u);
		cust.setCardType(cr.getCardtype());
		cust.setCcnumber(cr.getCardNum());
		cust.setExpiryDate(cr.getExpirydate());
		cust.setCvv(cr.getCvv());		
		session.update(cust);
		
		try {
			
		} catch (Exception ex) {
			return false;
		}
	return true;
	}
}
