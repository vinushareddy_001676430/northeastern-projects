package neu.edu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.controller.data.CustomersResp;

@Service
public class custdao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public  List<String>  list(String sellid) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Query query = session.createSQLQuery("Select c.UserName from Seller s" 
											+"  inner join product p on s.SellerID = p.SellerID"
											+" inner join orderitem oi on p.ProductID = oi.ProductID"
											+" inner join bestbuy.order o on oi.OrderID = o.OrderID"
											+" inner join customer c on o.CustomerID = c.CustomerID"
											+" where s.SellerID =:sellid"
											+" group by c.CustomerID"
											+" order by count(o.orderID) Desc"
											+" limit 2");
		query.setString("sellid", sellid);
		
		
		 List<String>  custs = ( List<String>) query.list();
				
		return custs;
	}
}
