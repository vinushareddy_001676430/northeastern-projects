package neu.edu.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.mapping.Customer;
import neu.edu.mapping.Order;
import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;

import neu.edu.model.OrderItemList;

@Service
public class OrderDao {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	
//	
//	@Transactional
//	public Order placeOrder(Order o){
//	Session session = sessionFactory.getCurrentSession();
//				
//	session.persist(o);		
//	//session.persist(oi);
//		
//		return o;
//		
//	}
	
	@Transactional
	public boolean setoi(List<Orderitem> oi,String custid,Order o){
	Session session = sessionFactory.getCurrentSession();

	session.save(o);
	for(Orderitem item: oi){
		Customer c=new Customer();
		c.setCustomerId(Integer.valueOf(custid));
		item.setCustomer(c);		
		session.update(item);
	}
		
	//session.persist(oi);
		
		return true;
		
	}
	
	public List<Object[]> listOrders(String custid) {
	
		Session session = sessionFactory.openSession();
//		Query query = session.createQuery("from Order where customerId=:custid");		
		

		Query query = session.createSQLQuery("select o.OrderID,p.ProductName,oi.Quantity,o.Status from product p"
												+"  inner join orderitem oi on p.ProductID = oi.ProductID"
												+"  inner join bestbuy.order o on oi.OrderID = o.OrderID"
												+"  inner join customer c on o.CustomerID = c.CustomerID"
												+"  where c.CustomerID =:custid");
		
		query.setString("custid",custid);
		List<Object[]> orders = (List<Object[]>) query.list();		
		
		
		try {
			session.close();
		} catch (Exception ex) {
			return null;
		}
		return orders;//		try {
//			session.close();
//		} catch (Exception ex) {
//			return null;
//		}
//		return ord;
//	}
}
}
