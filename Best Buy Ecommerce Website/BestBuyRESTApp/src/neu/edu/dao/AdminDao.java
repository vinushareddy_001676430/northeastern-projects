package neu.edu.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.controller.data.CardUpdaterequest;
import neu.edu.controller.data.EnableSellerRequest;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Seller;
import neu.edu.mapping.User;

@Service
	public class AdminDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Transactional
	public boolean enable(EnableSellerRequest e,int id) {
	
		
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("from Seller where sellerId=:id");
		
		query.setInteger("id", id);
		Seller seller= (Seller) query.uniqueResult();
		
		//cust.getCustomerId();	
		seller.setStatus(e.getStatus());
		session.persist(seller);
						
			
			return true;
		
		
	}
	@Transactional
	public boolean disable(EnableSellerRequest e,int id) {
	
		
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("from Seller where sellerId=:id");
		
		query.setInteger("id", id);
		Seller seller= (Seller) query.uniqueResult();
		
		//cust.getCustomerId();	
		seller.setStatus(e.getStatus());
		session.persist(seller);
						
			
			return true;
		
		
	}
}
