package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.OrderItemResponse;
import neu.edu.controller.data.ResponseError;
import neu.edu.controller.data.SellerResponse;
import neu.edu.service.OrderItemService;
import neu.edu.service.SellerService;

@Path("/displaysellers")
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdminController {
	
	@Autowired
	private SellerService sellService;
	
	@GET
	@PermitAll	
	public Response getSellers() {

		List<SellerResponse> ois = null;
		
		

			ois = sellService.getSellersList();
			
	

		if (ois == null) {
			return Response.ok().status(422).entity(new ResponseError("Sellers Not Found")).build();

		}

		
		return Response.ok().entity(ois).build();

	}
	
	
}
