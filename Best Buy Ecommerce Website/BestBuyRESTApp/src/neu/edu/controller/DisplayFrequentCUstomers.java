package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.CardDetailsResponse;
import neu.edu.controller.data.CustomersResp;
import neu.edu.controller.data.ResponseError;
import neu.edu.service.CreditCardService;
import neu.edu.service.CustomerService;

@Path("/displayFreqCusts/{sellerid}")
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DisplayFrequentCUstomers {
	
	@Autowired
	private CustomerService cdservice;
	
	@GET
	@PermitAll	
	public Response getcarddetails(@PathParam("sellerid") String sellerid) {
	List<CustomersResp> cds = null;
	

			cds = cdservice.getcusts(sellerid);
			
	System.out.println("hi1 from display");

		if (cds == null) {
			return Response.ok().status(422).entity(new ResponseError("Response Not Found")).build();

		}

		return Response.ok().entity(cds).build();

	}


}
