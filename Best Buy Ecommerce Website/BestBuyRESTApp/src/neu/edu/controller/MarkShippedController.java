package neu.edu.controller;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.ResponseError;
import neu.edu.service.OrderItemService;


@Path("/seller/markshipped/{orditemid}")	
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MarkShippedController {

	@Autowired
	private OrderItemService serv;

	@POST
	@PermitAll
		public Response markShipped(@PathParam("orditemid") String id) {
		boolean flag = false;
		
		try {
			flag =serv.markshipped(id);
		} catch (NumberFormatException ex) {
			return Response.ok().status(422).entity(new ResponseError("Invalid OrderItem Selection")).build();
		}
		
		if(flag){
			return Response.ok().build();
		}else{
			return Response.ok().status(422).entity(new ResponseError("Cannot Update Status")).build();

		}

	}
	
}
