package neu.edu.controller;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.CardUpdaterequest;
import neu.edu.controller.data.OrderItemRequest;
import neu.edu.controller.data.ResponseError;
import neu.edu.service.CreditCardService;
import neu.edu.service.OrderItemService;

@Path("/updateCardDetails/{custid}/{uname}")	
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UpdateCardDetails {

	
	@Autowired
	private CreditCardService cserv;

	@PUT
	@PermitAll	
	public Response addOrderItem(@PathParam("custid") String custId,@PathParam("uname") String uname,CardUpdaterequest request) {
		boolean flag = false;
//		try {
			
			flag = cserv.update(request,uname,custId);
//		} catch (NumberFormatException ex) {
//			return Response.ok().status(422).entity(new ResponseError("Invalid UserId Format")).build();
//		}
		
		if(flag){
			return Response.ok().build();
		}else{
			return Response.ok().status(422).entity(new ResponseError("Update Failed")).build();

		}

	}
}
