package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.OrderItemRequest;
import neu.edu.controller.data.OrderItemResponse;
import neu.edu.controller.data.ResponseError;
import neu.edu.mapping.Order;
import neu.edu.mapping.Orderitem;
import neu.edu.model.OrderItemList;
import neu.edu.service.OrderItemService;
import neu.edu.service.OrderService;

@Path("/Customer/{custId}")	
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AddOrderController {

	
	@Autowired
	private OrderService oService;

	@POST
	@PermitAll
	@Path("/addOrder")
	

	public Response addOrder(List<OrderItemResponse> ordlist,@PathParam("custId") String custId) {
		Order or=null;
		boolean flag1;
		try {
			//or = oService.placeOrder(Integer.parseInt(custId));
			
	
			
			
			 flag1=oService.setoi(ordlist,custId);
			
			
			//oService.setOrderinOrdItem(oi);
			//for(int i=0;i<oil.getOis().size()-1;)
			
		} catch (NumberFormatException ex) {
			return Response.ok().status(422).entity(new ResponseError("Invalid custId Format, Login Required")).build();
		}
		
		if(flag1){
			return Response.ok().build();
		}else{
			return Response.ok().status(422).entity(new ResponseError("Order Creation Failed")).build();

		}

	}	
	
}
