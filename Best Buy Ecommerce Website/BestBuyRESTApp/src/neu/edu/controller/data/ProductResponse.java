package neu.edu.controller.data;

public class ProductResponse {
	
	private Integer productId;
	private String productName;
	private String Description;
	private float Price;
	private Integer quantity;
	
	public ProductResponse(String prodName,String Desc,Float price,Integer productid){
		
		this.productName=prodName;
		this.Description=Desc;
		this.Price=price;
		this.productId=productid;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public float getPrice() {
		return Price;
	}
	public void setPrice(float price) {
		Price = price;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	
	
	
}
