package neu.edu.controller.data;

public class NewOrderItemResponse {

	private Integer orderitemid;
	private Integer quantity;
	private Integer orderid;
	private String prodName;
	private String orderStatus;
	public NewOrderItemResponse(){
		
	}
	
	public NewOrderItemResponse(Integer orderitemid,Integer quantity,Integer orderid,String prodName,String orderStatus){
		this.orderitemid=orderitemid;
		this.quantity=quantity;
		this.orderid=orderid;
		this.prodName=prodName;
		this.orderStatus=orderStatus;
	}
	public Integer getOrderitemid() {
		return orderitemid;
	}
	public void setOrderitemid(Integer orderitemid) {
		this.orderitemid = orderitemid;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getOrderid() {
		return orderid;
	}
	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	
}
