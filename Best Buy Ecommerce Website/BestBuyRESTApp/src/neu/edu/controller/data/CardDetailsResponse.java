package neu.edu.controller.data;

public class CardDetailsResponse {

	private String cardtype;
	private Integer cvv;
	private Integer cardNum;
	private String expirydate;
	private String username;
	
	public CardDetailsResponse(String cardtype,Integer cardNum,String Expirydate,Integer cvv,String username){
		this.cardtype=cardtype;
		this.cardNum=cardNum;
		this.expirydate=Expirydate;
		this.cvv=cvv;
		this.username=username;
		
	}
	public CardDetailsResponse(){
		
	}
	public String getCardtype() {
		return cardtype;
	}

	public void setCardtype(String cardtype) {
		this.cardtype = cardtype;
	}

	public Integer getCvv() {
		return cvv;
	}

	public void setCvv(Integer cvv) {
		this.cvv = cvv;
	}

	public Integer getCardNum() {
		return cardNum;
	}

	public void setCardNum(Integer cardNum) {
		this.cardNum = cardNum;
	}

	public String getExpirydate() {
		return expirydate;
	}

	public void setExpirydate(String expirydate) {
		this.expirydate = expirydate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
