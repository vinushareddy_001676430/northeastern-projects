package neu.edu.controller.data;

import neu.edu.mapping.User;

public class SellerResponse {

	private Integer sellerId;
	private String username;
	private String merchandiseName;
	private String status;
	
	public SellerResponse(int sellerid,String uname,String merchname,String status){
		this.sellerId=sellerid;
		this.username=uname;
		this.merchandiseName=merchname;
		this.status=status;
	}

	public Integer getSellerId() {
		return sellerId;
	}

	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMerchandiseName() {
		return merchandiseName;
	}

	public void setMerchandiseName(String merchandiseName) {
		this.merchandiseName = merchandiseName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
