package neu.edu.controller.data;

public class ProductRequest {

	private String productName;
	private String Description;
	private float Price;
	private String Weight;
	private String Type;

	
	
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public float getPrice() {
		return Price;
	}
	public void setPrice(float price) {
		Price = price;	
	}
	public String getWeight() {
		return Weight;
	}
	public void setWeight(String weight) {
		Weight = weight;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	}
