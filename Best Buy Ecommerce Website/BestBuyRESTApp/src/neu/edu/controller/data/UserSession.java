package neu.edu.controller.data;

public class UserSession{
	private int id;
	private String name;
	private String role;
	private boolean success = false;
	private String message = "";
	
//	private boolean success=true;
	
	public UserSession(){
		
	}
	
	
	public UserSession(int id, String name, String role) {
		super();
		this.id = id;
		this.name = name;
		this.role = role;
	}
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}
	
}