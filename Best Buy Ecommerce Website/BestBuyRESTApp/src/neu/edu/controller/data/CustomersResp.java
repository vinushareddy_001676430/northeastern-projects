package neu.edu.controller.data;


public class CustomersResp {

	private String custname;
	
	public CustomersResp(String custname){
		this.custname=custname;
}

	public String getCustname() {
		return custname;
	}

	public void setCustname(String custname) {
		this.custname = custname;
	}

	
}