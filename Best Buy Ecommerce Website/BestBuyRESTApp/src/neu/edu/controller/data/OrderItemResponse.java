package neu.edu.controller.data;

public class OrderItemResponse {
	
	private Integer oiId;
	private Integer quantity;
	//private Integer orderId;
	private Integer productId;
	private Integer custid;
	
	public OrderItemResponse(){
		
	}
	
	public OrderItemResponse(int id,int qty,int prodid,int custid){
		this.oiId=id;
		this.quantity=qty;
		this.productId=prodid;
		this.custid=custid;
	}
	
	public Integer getOiId() {
		return oiId;
	}
	public void setOiId(Integer oiId) {
		this.oiId = oiId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getCustid() {
		return custid;
	}

	public void setCustid(Integer custid) {
		this.custid = custid;
	}


	
}
