package neu.edu.controller.data;

public class OrderResponse {

	private String status;
	private Integer orderid;
	private String prodname;
	private Integer qty;
	//private String Date;
	
	public OrderResponse(Integer orderid,String stat,String name,int qty){
		this.status=stat;
		this.orderid=orderid;
		this.prodname=name;
		this.qty=qty;
				
		//this.Date=Date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getOrderid() {
		return orderid;
	}

	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	public String getProdname() {
		return prodname;
	}

	public void setProdname(String prodname) {
		this.prodname = prodname;
	}

	public float getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
	
	
}
