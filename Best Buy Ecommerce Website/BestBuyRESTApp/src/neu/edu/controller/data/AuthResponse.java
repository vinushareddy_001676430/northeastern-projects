package neu.edu.controller.data;

public class AuthResponse {

	private String userName;
	private String role;
	
//	private boolean success = false;
//	private String message = "";
	
	
	public AuthResponse(String userName,String role){
		this.role=role;
		this.userName=userName;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
	
}
