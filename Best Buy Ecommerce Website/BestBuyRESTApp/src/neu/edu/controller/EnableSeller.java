package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.EnableSellerRequest;
import neu.edu.controller.data.OrderItemResponse;
import neu.edu.controller.data.ResponseError;
import neu.edu.mapping.Order;
import neu.edu.service.AdminService;
import neu.edu.service.OrderService;

@Path("/Admin/enable/{sellerId}")	
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EnableSeller {
	
	@Autowired
	private AdminService Service;

	@POST
	@PermitAll
	public Response enable(EnableSellerRequest e,@PathParam("sellerId") String id) {
		
		boolean flag1;
		try {
			
			 flag1=Service.enabled(e,id);
			
			
			
		} catch (NumberFormatException ex) {
			return Response.ok().status(422).entity(new ResponseError("Invalid custId Format, Login Required")).build();
		}
		
		if(flag1){
			return Response.ok().build();
		}else{
			return Response.ok().status(422).entity(new ResponseError("Order Creation Failed")).build();

		}

	}	
	
}
