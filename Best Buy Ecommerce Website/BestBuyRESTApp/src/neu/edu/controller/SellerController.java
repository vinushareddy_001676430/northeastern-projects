package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.NewOrderItemResponse;
import neu.edu.controller.data.ProductResponse;
import neu.edu.controller.data.ResponseError;
import neu.edu.service.ProductService;

@Path("/seller/{sellerid}")
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class SellerController {
	
	@Autowired
	private ProductService productService;
	
	@GET
	@PermitAll	
	public Response getProducts(@PathParam("sellerid") String sellerid) {

		List<NewOrderItemResponse> ois = null;

			ois = productService.getOrderItems(sellerid);

		

		if (ois == null) {
			return Response.ok().status(422).entity(new ResponseError("OrderItems Not Found")).build();

		}
		return Response.ok().entity(ois).build();

	}

}
