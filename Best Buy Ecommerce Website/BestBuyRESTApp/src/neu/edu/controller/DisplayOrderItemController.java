package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.OrderItemResponse;
import neu.edu.controller.data.ResponseError;
import neu.edu.service.OrderItemService;

@Path("/displayOrderItems/{custid}")
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DisplayOrderItemController {

	@Autowired
	private OrderItemService oiService;
	
	@GET
	@PermitAll	
	public Response getOrderItems(@PathParam("custid") String custid) {

		List<OrderItemResponse> ois = null;
		
		System.out.println("The is oi sevcice"+custid);

			ois = oiService.getOiList(custid);
			
	

		if (ois == null) {
			return Response.ok().status(422).entity(new ResponseError("Order Items Not Found")).build();

		}

		
		return Response.ok().entity(ois).build();

	}
	
	
}
