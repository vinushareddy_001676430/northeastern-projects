package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.ProductRequest;
import neu.edu.controller.data.RegistrationRequest;
import neu.edu.controller.data.ResponseError;
import neu.edu.controller.data.RestLogicalErrorException;

import neu.edu.service.ProductService;
import neu.edu.service.RegistrationService;

@Path("/seller/{id}")	
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class AddingProductsController {
	
	@Autowired
	private ProductService productService;

	@POST
	@PermitAll
	@Path("/Product")	
	public Response addProduct(@PathParam("id") String id,ProductRequest prodrequest) {
		boolean flag = false;
		
		try {
			flag = productService.createProduct(prodrequest, Integer.parseInt(id));
		} catch (NumberFormatException ex) {
			return Response.ok().status(422).entity(new ResponseError("Invalid UserId Format")).build();
		}
		
		if(flag){
			return Response.ok().build();
		}else{
			return Response.ok().status(422).entity(new ResponseError("Resume Creation Failed")).build();

		}

	}
	



}
