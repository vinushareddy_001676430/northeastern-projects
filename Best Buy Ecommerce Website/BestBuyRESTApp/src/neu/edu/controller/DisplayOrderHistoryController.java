package neu.edu.controller;

import java.util.List;


import javax.annotation.security.PermitAll;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.OrderResponse;
import neu.edu.controller.data.ResponseError;

import neu.edu.service.OrderService;

@Path("/displayOrdHistory/{custid}")
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DisplayOrderHistoryController {

	@Autowired
	private OrderService ordService;
	
	@GET
	@PermitAll	
	public Response getOrderItems(@PathParam("custid") String custid) {

		List<OrderResponse> ol = null;

			ol = ordService.getOrdList(custid);
			
	

		if (ol == null) {
			return Response.ok().status(422).entity(new ResponseError("Order History Not Found")).build();

		}

		
		return Response.ok().entity(ol).build();

	}
	
	
}
