package neu.edu.controller;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import neu.edu.controller.data.ProductRequest;
import neu.edu.controller.data.ProductResponse;
import neu.edu.controller.data.ResponseError;
import neu.edu.service.ProductService;



@Path("/displayProducts")
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class DisplayProductController {

	@Autowired
	private ProductService productService;
	
	@GET
	@PermitAll	
	public Response getProducts() {

		List<ProductResponse> prods = null;

			prods = productService.getProductsList();

		

		if (prods == null) {
			return Response.ok().status(422).entity(new ResponseError("Products Not Found")).build();

		}
		return Response.ok().entity(prods).build();

	}

	
}
