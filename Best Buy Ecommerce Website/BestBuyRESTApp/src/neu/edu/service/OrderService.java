package neu.edu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.OrderItemRequest;
import neu.edu.controller.data.OrderItemResponse;
import neu.edu.controller.data.OrderResponse;
import neu.edu.dao.OrderDao;
import neu.edu.dao.OrderItemDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Order;
import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;
import neu.edu.model.OrderItemList;

@Service
public class OrderService {

	@Autowired
	private OrderDao oDao;
	
	

public boolean setoi(List<OrderItemResponse> ol,String custId){
	
	//OrderItemList oil=new OrderItemList();
	
	Order ord=new Order();
	ord.setStatus("Order Placed");
	//ord.setOrderedDate(orderedDate);
	//oiobj.setQuantity(oi.getQuantity());
	
	Customer c=new Customer();
	c.setCustomerId(Integer.parseInt(custId));
	ord.setCustomer(c);
	
	List<Orderitem> entityoilist = new ArrayList<>();
	
	for(OrderItemResponse oiresponse:ol){
		
		//oiresponse.setOrderId(o.getOrderId());
	
		Orderitem orditem=new Orderitem();
		
		orditem.setOrderItemId(oiresponse.getOiId());
		orditem.setQuantity(oiresponse.getQuantity());
		
		Product p=new Product();
		p.setProductId(oiresponse.getProductId());
		
		orditem.setProduct(p);
		orditem.setOrder(ord);
		entityoilist.add(orditem);		
		//ol.getOis().add(orditem);
	
	}
	//for(Orderitem orditem:entityoilist){
		oDao.setoi(entityoilist,custId,ord);
//	}
	
	//return oDao.setoi(o);
	return true;
}

public List<OrderResponse> getOrdList(String custid){

	List<Object[]> orders = oDao.listOrders(custid);
	List<OrderResponse> ordResponse = null;
	if (orders != null) {
		//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		ordResponse = new ArrayList<>();
		for (Object[] row : orders) {
			OrderResponse response = new OrderResponse((Integer)row[0],(String)row[3],(String)row[1],(int)row[2]); 
			
			ordResponse.add(response);
		}
	}
	return ordResponse;
	
}
	
}
