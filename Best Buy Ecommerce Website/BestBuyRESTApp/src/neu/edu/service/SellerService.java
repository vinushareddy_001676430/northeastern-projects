package neu.edu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.ProductResponse;
import neu.edu.controller.data.SellerResponse;
import neu.edu.dao.ProductDao;
import neu.edu.dao.SellerDao;
import neu.edu.mapping.Product;
import neu.edu.mapping.Seller;

@Service
public class SellerService {

	@Autowired
	private SellerDao sellerDao;
	
	
	public List<SellerResponse> getSellersList(){

		List<Seller> sellers = sellerDao.listSellers();
		List<SellerResponse> Response = null;
		if (sellers != null) {
			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Response = new ArrayList<>();
			for (Seller seller: sellers) {
				SellerResponse response = new SellerResponse(seller.getSellerId(),seller.getUser().getUserName(),seller.getMerchandiseName(),seller.getStatus());
														
				
				Response.add(response);
			}
		}
		return Response;
		
	}
}
