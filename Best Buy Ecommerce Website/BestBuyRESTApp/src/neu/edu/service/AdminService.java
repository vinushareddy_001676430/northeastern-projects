package neu.edu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.EnableSellerRequest;
import neu.edu.controller.data.ProductRequest;
import neu.edu.dao.AdminDao;
import neu.edu.dao.SellerDao;
import neu.edu.mapping.Product;
import neu.edu.mapping.Seller;

@Service
public class AdminService {


	@Autowired
	private AdminDao Dao;
	
	public boolean enabled(EnableSellerRequest Request,String sellerId){
		
		int sellerI1d=Integer.parseInt(sellerId);
		return Dao.enable(Request,sellerI1d);
		
	}
	
public boolean disabled(EnableSellerRequest Request,String sellerId){
		
		int sellerI1d=Integer.parseInt(sellerId);
		return Dao.disable(Request,sellerI1d);
		
	}
	
}
