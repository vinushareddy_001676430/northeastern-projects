package neu.edu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.CustomersResp;
import neu.edu.controller.data.OrderItemResponse;
import neu.edu.dao.OrderItemDao;
import neu.edu.dao.custdao;

@Service
public class CustomerService {
	@Autowired
	private custdao oiDao;
	
	public List<CustomersResp> getcusts(String sellid){

		List<String> ois = oiDao.list(sellid);
		List<CustomersResp> oiResponse = null;
		
		if (ois != null) {
		
			oiResponse = new ArrayList<>();
				for(String c:ois){				
				
					CustomersResp response = new CustomersResp(c);
				
					oiResponse.add(response);
				}
			}
		
		return oiResponse;
		
	}
}
