package neu.edu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.CardDetailsResponse;
import neu.edu.controller.data.CardUpdaterequest;
import neu.edu.controller.data.OrderItemRequest;
import neu.edu.controller.data.OrderItemResponse;
import neu.edu.dao.CardDao;
import neu.edu.dao.OrderItemDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Orderitem;
import neu.edu.mapping.Product;

	@Service
	public class CreditCardService {
		
	@Autowired
	private CardDao dao;
	
	
	public CardDetailsResponse getCreditcard(String custid){

		Object[] ois =dao.listcarddetails(custid); 
		CardDetailsResponse response =null;
		
		if (ois != null) {
		
		
							
				
					 response = new CardDetailsResponse((String)ois[5],(Integer)ois[2],(String)ois[3],(Integer)ois[4],(String)ois[0]);
				
					
				
			}
		
		return response;
		
	}
	
	public boolean update(CardUpdaterequest cr,String custid,String uname){

		
		boolean ois =dao.updatecard(cr,uname,custid);
		
		return ois;
		
	}
	
	
}
