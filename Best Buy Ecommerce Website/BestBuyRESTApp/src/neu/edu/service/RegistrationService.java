package neu.edu.service;

import java.util.Set;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import neu.edu.controller.data.RegistrationRequest;
import neu.edu.controller.data.RestLogicalErrorException;
import neu.edu.controller.data.md5;
import neu.edu.dao.UserDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Seller;
import neu.edu.mapping.User;
import sun.security.provider.MD5;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class RegistrationService {
	
	@Autowired
	private UserDao userDao;
	
	
	public boolean registerCustomer(RegistrationRequest registrationRequest) throws RestLogicalErrorException{
		
		if(registrationRequest.getUsername() ==null ||
		   registrationRequest.getPassword() ==null){
			
			throw new RestLogicalErrorException("Registration Parameters incomplete.");
		}else{
			//Simulation a database Request
			Customer customer=new Customer();
			
		
			User user=new User();
			
			user.setFirstName(registrationRequest.getFirstName());
			user.setPassword(md5.md5(registrationRequest.getPassword()));
			user.setUserName(registrationRequest.getUsername());
			
			
			user.setRole("customer");
			user.setLastName(registrationRequest.getLastName());
			user.setAddressLine1(registrationRequest.getAddressLine1());
			user.setAddressLine2(registrationRequest.getAddressLine2());
			user.setCity(registrationRequest.getCity());
			user.setState(registrationRequest.getState());
			user.setZip(registrationRequest.getZip());
			user.setTitle(registrationRequest.getTitle());
			user.setEmail(registrationRequest.getEmail());
			user.setPhone(registrationRequest.getPhone());
		

			customer.setUser(user);
			customer.setCardType(registrationRequest.getCardtype());
			customer.setCcnumber(registrationRequest.getCcnumber());
			customer.setExpiryDate(registrationRequest.getExpirydate());
			customer.setCvv(registrationRequest.getCvv());
		
			user.getCustomers().add(customer);
			
			

			if(!userDao.registrationCustomer1(user,customer)){

				
				throw new RestLogicalErrorException("Duplicate User.");

			}
		}
		
		
		return true;
		
	}
	
	public boolean registerSeller(RegistrationRequest registrationRequest) throws RestLogicalErrorException{
		
		if(registrationRequest.getUsername() ==null ||
		   registrationRequest.getPassword() ==null){
			
			throw new RestLogicalErrorException("Registration Parameters incomplete.");
		}else{
			
			Seller seller=new Seller();
			
			
			User user=new User();

			user.setFirstName(registrationRequest.getFirstName());
			user.setPassword(md5.md5(registrationRequest.getPassword()));
			user.setUserName(registrationRequest.getUsername());
			
			user.setRole("seller");
			user.setLastName(registrationRequest.getLastName());
			user.setAddressLine1(registrationRequest.getAddressLine1());
			user.setAddressLine2(registrationRequest.getAddressLine2());
			user.setCity(registrationRequest.getCity());
			user.setState(registrationRequest.getState());
			user.setZip(registrationRequest.getZip());
			user.setTitle(registrationRequest.getTitle());
			user.setEmail(registrationRequest.getEmail());
			user.setPhone(registrationRequest.getPhone());
		

			seller.setUser(user);
			seller.setMerchandiseName(registrationRequest.getMerchandiseName());
		
			user.getSellers().add(seller);

			if(!userDao.registrationSeller(user,seller)){

				
				throw new RestLogicalErrorException("Duplicate User.");

			}
		}
		
		
		return true;
		
	}
	
	

}
