package neu.edu.service;

import java.util.Iterator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import neu.edu.controller.data.AuthResponse;
import neu.edu.controller.data.RestLogicalErrorException;
import neu.edu.controller.data.UserSession;
import neu.edu.dao.UserDao;
import neu.edu.mapping.Customer;
import neu.edu.mapping.Seller;
import neu.edu.mapping.User;

@Service
public class AuthenticationService {

	@Autowired
	private UserDao userDao;

	// public AuthResponse validateCustomer(String username,String password,
	// String role){
	//
	// User user= userDao.validateUser(username, password, role);
	// AuthResponse authResponse=new
	// AuthResponse(user.getUserName(),user.getRole());
	//
	// return authResponse;
	// }

	@Transactional
	public UserSession validateUser(String username, String password) throws RestLogicalErrorException {

		UserSession userSession = userDao.validateUser(username, password);


		if (userSession != null) {
			return userSession;
		} 
		else {
			RestLogicalErrorException authResponseErr = new RestLogicalErrorException("Invalid User");
			throw authResponseErr;
		}

	}
}
