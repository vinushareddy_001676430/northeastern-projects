-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bestbuy
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `OrderID` int(15) NOT NULL AUTO_INCREMENT,
  `OrderedDate` date DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `CustomerID` int(11) NOT NULL,
  PRIMARY KEY (`OrderID`),
  KEY `IX_Relationship16` (`CustomerID`),
  CONSTRAINT `Relationship16` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (2,NULL,'Order Placed',1),(3,NULL,'Order Placed',1),(4,NULL,'Order Placed',1),(5,NULL,'Order Placed',1),(8,NULL,'Order Placed',1),(9,NULL,'Order Placed',1),(10,NULL,'Order Placed',1),(11,NULL,'Order Placed',1),(12,NULL,'Order Placed',2),(13,NULL,'Order Placed',2),(14,NULL,'Order Placed',2),(15,NULL,'Order Placed',2),(16,NULL,'Order Placed',2),(17,NULL,'Order Placed',1),(18,NULL,'Order Placed',2),(20,NULL,'Order Placed',2),(21,NULL,'Order Placed',2),(22,NULL,'Order Placed',2),(23,NULL,'Order Placed',2),(24,NULL,'Order Placed',2),(25,NULL,'Order Placed',2),(26,NULL,'Order Placed',2),(27,NULL,'Order Placed',2),(28,NULL,'Order Placed',2),(29,NULL,'Order Placed',2),(30,NULL,'Order Placed',2),(31,NULL,'Order Placed',2),(32,NULL,'Order Placed',4),(33,NULL,'Order Placed',4),(34,NULL,'Order Placed',4),(35,NULL,'Order Placed',4),(36,NULL,'Order Placed',4),(37,NULL,'Order Placed',4),(38,NULL,'Order Placed',10),(39,NULL,'Order Placed',10),(40,NULL,'Order Placed',10);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-15  8:12:55
