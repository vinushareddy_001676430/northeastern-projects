-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bestbuy
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `UserName` varchar(20) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Role` char(20) NOT NULL,
  `FirstName` varchar(20) NOT NULL,
  `LastName` varchar(20) DEFAULT NULL,
  `Title` varchar(20) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Phone` int(50) DEFAULT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `AddressLine1` varchar(40) DEFAULT NULL,
  `AddressLine2` varchar(30) DEFAULT NULL,
  `City` char(20) DEFAULT NULL,
  `State` char(20) DEFAULT NULL,
  `ZIP` int(15) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('','pass','seller','','','',NULL,NULL,'','','','','',NULL),('admin','pass','admin','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ammulu','sai','customer','Vinusha Reddy',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ammulu1','sai','customer','Vinusha Reddy Ma',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('apooja','poo','seller','','','',NULL,NULL,'','','','','',NULL),('h','h','customer','h','h','Mr',NULL,7,'h','h','hh','h','h',8),('hari','har','customer','Harivansh','Reddy','Mr',NULL,65685,'hari@gmail.com','HNo2­22­5 EenadCly MainRd KKP','sxdsw','Hyderabad','Telangana',500072),('hii','ii','customer','u','yt','Mrs',NULL,87,'uh','t','u','uy','yt',767),('jh','jh','customer','jkh','jh','Ms',NULL,87,'jhj','hj','hj','hjh','jh',7786),('joe','joey','seller','John',NULL,'Mr.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Johnson','joey','seller','John','han','Mr.',NULL,98873,'joe.han@gmail.com','#35','98 Darling Street','Arlington','Mass',2997),('neha','262f5bdd0af9098e7443ab1f8e435290','customer','neha','pradhan','Ms',NULL,87867,'neha@gmail.com','#11,49 symphony road','uh','Boston','Massachusetts',2115),('pruthvi','pru','customer','Pruthvi','Muddapuram','Ms',NULL,8574002,'pruthviraj@gmail.com','Apt #8','1171 Boylston street','Boston','Massachusetts',2115),('rathi','rats','customer','rathi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('rats','b963deb57bb3b8eaa7845c0cfe69f525','seller','rathi','rath','Ms',NULL,87676,'rats@gmail.com','Apt #8','1171 Boylston street','Boston','Massachusetts',2115),('seller','sell','seller','seller',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('seller1','seller','seller','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('shalini','sha','seller','Shalini','Reddy','Mrs.',NULL,857498,'yuu@gyh.com','','','','',NULL),('ty','36f3af6226e0b5303e19b824e7442272','customer','yt','yt','Mr',NULL,887,'ty','yt','yt','yt','yt',87),('uh','ok','seller','hk','hjuh','Mr',NULL,78,'uihy','uy','uy','uyuy','uy',887),('uma','f5c2db1f19bdde37e740e86b70d0534f','seller','uma','devi','Ms',NULL,7876,'umadevi','49 symphony road','49 symphony road','Boston','Massachusetts',2115),('uname','sai1','customer','Vins redd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('vin','pass2j','cuskjt','Vinushi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('y','y','customer','y','y','Ms',NULL,7,'y','f','f','f','7',6);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-15  8:12:55
