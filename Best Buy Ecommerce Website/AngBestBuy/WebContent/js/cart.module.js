/**
 * 
 */

var cartModule = angular.module("cart.module");

cartModule.controller('cartController', function($scope,$uibModal,$rootScope,$location,cartService) {
	var cartCtrl = this;
	$scope.messageDash = "This is View Cart page";

	$scope.back = function() {
		$location.path("/dashboard");
	}
	
	$scope.edit = function() {
		document.getElementById("cardtype").disabled = false;
		document.getElementById("cardnum").disabled = false;
		document.getElementById("cvv").disabled = false;
		document.getElementById("expdate").disabled = false;
		document.getElementById("edit").disabled = true;
		document.getElementById("update").disabled = false;
	}
	
	$scope.update = function() {
		console.log($rootScope.globals.Customer.custid);
	
	cartService.updateinfo($scope.custid,$rootScope.globals.Customer.username,cartCtrl.card,callback,callbackDashboardListError);
	}
	
	$scope.init = function () {
		cartService.listOfOrderItems($rootScope.globals.userSession.id,callback,callbackDashboardListError);
		cartService.listOfCardDetails($rootScope.globals.userSession.id,callbackcard,callbackDashboardListErrorcard);
		
		//cartService.listOfShippingAdd($rootScope.globals.userSession.name,callbackAddr,callbackErrorAddr);
	
	};
	
	$scope.checkout = function() {
		//console.log($rootScope.globals.OrderItemResponse.arrayobj);
		console.log($rootScope.globals.Customer.custid);
		cartService.checkout($scope.orderitems,$scope.custid,callbackcheckout,callbackDashboardListErrorcheckout);
	};

	cartCtrl.card={
			cardtype: '',
			cardnum: '',
			expirydate: '',
			cvv: ''
	};
	

	
	var callback = function(data,id,$rootScope) 
	{		
		
		console.log(id);
		$scope.custid=id;
		cartService.setCustomerCredentials(data);
		$scope.orderitems = data;
		//console.log($rootScope.globals.Customer.custid)
		console.log($scope.orderitems);
	
		
    };
    
    var callbackcard = function(data,$rootScope) 
	{		
    	console.log($scope.orderitems);
		$scope.carddetails = data;
		console.log($rootScope.globals.Customer.custid);
		console.log($rootScope.globals.Customer.username);
		console.log($scope.carddetails);
	
		
    };
    
    var callbackupdate = function(data,$rootScope) 
	{	
    	cartCtrl.openComponentModal('Updated Sucessfully');
		console.log("update sucesss");
	
    };
    
    var callbackDashboardListErrorupdate = function(data,$rootScope) 
	{	
		console.log("update failed");
	
    };
    
    var callbackDashboardListErrorcard = function(data,headers) {
		console.log("error");
//    	$scope.message = data.message;
//		$scope.error = true;   
};
    
    var callbackDashboardListError = function(data,headers) {
    		$scope.message = data.message;
    		$scope.error = true;   
    };
    
    var callbackcheckout = function(data,$rootScope) 
	{		
    	
    	if($scope.orderitems.length==0){
    	
    	cartCtrl.openComponentModal('Please fill your cart to checkout');
    		
    	}
    	else{
		console.log($scope.orderitems);
		console.log($rootScope.globals.Customer.custid);
		cartCtrl.openComponentModal('Order placed sucessfully');
				
    	}
    }; 
    
    var callbackDashboardListErrorcheckout = function(data,headers) {
	console.log("checkout failed") 
    };
    
    cartCtrl.openComponentModal = function(msgToDisplay) {
		var modalInstance = $uibModal.open({
			animation : true,
			component : 'successComponent',
			resolve : {
				msg : function() {
					return msgToDisplay;
				}
			}
		});

	};
});


cartModule.service('cartService', function($rootScope,$http,$timeout,APP_CONSTANT) {
	var cartService = {};
	
	cartService.listOfOrderItems =  function(id,callback,callbackError) {
			if (APP_CONSTANT.DEMO) {
				console.log('ID -->'+id);
				
				$timeout(function() {

					var response;
					
					response = [
										{resumeId:12,name:"Coming to US",desc:"-",dateOfCreation:'20/Dec/2016'},
										{resumeId:23,name:"After Semester 1",desc:"testing app",dateOfCreation:'18/May/2016'},
										{resumeId:43,name:"For Co-op",desc:"",dateOfCreation:'1/Nov/2016'}
								];
					

					callback(response);
				}, 1000);
			} else {
				console.log('ID -->'+id);
				$http.get(APP_CONSTANT.REMOTE_HOST +'/displayOrderItems/'+id)
				// On Success of $http call
				.success(function(data, status, headers, config) {
					
					$rootScope.globals = {
		 					OrderItemResponse: {
		 					arrayobj:data
		 				 },
					Customer:{
						custid:id						
					}
					};
					callback(data,id,$rootScope);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackError(data, headers);
				});
			}

		};
		
		cartService.checkout =  function(data,custid,callback,callbackError) {
			if (APP_CONSTANT.DEMO) {
				console.log('ID -->'+id);
				
				$timeout(function() {

					var response;
					
					response = [
										{resumeId:12,name:"Coming to US",desc:"-",dateOfCreation:'20/Dec/2016'},
										{resumeId:23,name:"After Semester 1",desc:"testing app",dateOfCreation:'18/May/2016'},
										{resumeId:43,name:"For Co-op",desc:"",dateOfCreation:'1/Nov/2016'}
								];
					

					callback(response);
				}, 1000);
			} else {
				console.log('orderitemresponse obj '+data);
				$http.post(APP_CONSTANT.REMOTE_HOST +'/Customer/'+custid+'/addOrder/',data
						
				)
				// On Success of $http call
				.success(function(data, status, headers, config) {		
				
					callback(data,$rootScope);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackError(data, headers);
				});
			}

		};
		
       
		cartService.listOfCardDetails= function (custid,callbackcard,callbackDashboardListErrorcard){
			if (APP_CONSTANT.DEMO) {
				console.log('custID -->'+custid);
				
				$timeout(function() {

					var response;
					
					response = [
										{resumeId:12,name:"Coming to US",desc:"-",dateOfCreation:'20/Dec/2016'},
										{resumeId:23,name:"After Semester 1",desc:"testing app",dateOfCreation:'18/May/2016'},
										{resumeId:43,name:"For Co-op",desc:"",dateOfCreation:'1/Nov/2016'}
								];
					

					callback(response);
				}, 1000);
			} else {
				
				$http.get(APP_CONSTANT.REMOTE_HOST +'/displayCardDetails/'+custid)
						
				
				// On Success of $http call
				.success(function(data, status, headers, config) {		
					$rootScope.globals = {
		 					Customer: {
		 					username:data.username
		 				 }
					};
					callbackcard(data,$rootScope);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackDashboardListErrorcard(data, headers);
				});
			}

		};
		

        cartService.setCustomerCredentials = function (data) {
        	 //Setting of Auth ID
    		
			$rootScope.globals = {
 					OrderItemResponse: {
 					arrayobj:data
 				 },
			Customer:{
				custid:data.custid
				
			}
			};
         console.log($rootScope.globals.OrderItemResponse.arrayobj);
 	     
    };
    
		cartService.updateinfo= function (custid,username,data,callbackupdate,callbackDashboardListErrorupdate){
			if (APP_CONSTANT.DEMO) {
				console.log('custID -->'+custid);
				
				$timeout(function() {

					var response;
					
					response = [
										{resumeId:12,name:"Coming to US",desc:"-",dateOfCreation:'20/Dec/2016'},
										{resumeId:23,name:"After Semester 1",desc:"testing app",dateOfCreation:'18/May/2016'},
										{resumeId:43,name:"For Co-op",desc:"",dateOfCreation:'1/Nov/2016'}
								];
					

					callback(response);
				}, 1000);
			} else {
				console.log('custID -->'+custid);	
				
				$http.put(APP_CONSTANT.REMOTE_HOST +'/updateCardDetails/'+username+'/'+custid,
						
						{
						
					cardtype:data.cardtype,
					cardNum:data.cardnum,
					expirydate:data.expirydate,
					cvv:data.cvv
						}
				
				
				)
						
				
				// On Success of $http call
				.success(function(data, status, headers, config) {		
				
					callbackupdate(data,$rootScope);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackDashboardListErrorupdate(data, headers);
				});
			}

		};
		

	
	return cartService;
})
