/**
 * 
 */

var oimodule = angular.module("orderitem.module");

oimodule.controller('oiController', function($scope,$rootScope,$location,$uibModal,oiService) {
	var oiCtrl = this;
	

	$scope.init = function () {
		 console.log($rootScope.globals.userSession.id);
		oiService.listOfOis($rootScope.globals.userSession.id,callback,callbackDashboardListError);
	
	};
	
	
	

//	dashCtrl.variables={	
//			quantity: ''
//	};			
//	
	oiCtrl.add=function(){
		console.log($rootScope.globals.userSession.id);
		$location.path('/add/prod');
		
	};
	
	$scope.markShipped = function (oiObj) {		
		
		console.log($rootScope.globals.userSession.id);
		console.log(oiObj.rowId);		
		oiService.mark(oiObj.rowId.orderitemid,callbackmark,callbackerrormark);
		
		

	};

	//doesnt need headers
	var callback = function(data) 
	{ // Status Code:200
		
		$scope.ois = data;
		
		//console.log("seller id new: "+$rootScope.globals.Seller.sellid);
		console.log($scope.ois);
		
    };
    
    var callbackDashboardListError = function(data,headers) {
    		$scope.message = data.message;
    		$scope.error = true;   
    };
	var callbackmark = function(data,headers) 
	{ 
		oiCtrl.openComponentModal('Marked as Shipped Sucessfully');
		console.log("sucesss");
    };
    
    var callbackerrormark = function(data,headers) {
    	$scope.message = data.message;
    	$scope.error = true;   
    	oiCtrl.openComponentModal('Could not Mark as Shipped');
    };
    
	
    oiCtrl.openComponentModal = function(msgToDisplay) {
		var modalInstance = $uibModal.open({
			animation : true,
			component : 'successComponent',
			resolve : {
				msg : function() {
					return msgToDisplay;
				}
			}
		});

	};
});


oimodule.service('oiService', function($http,$rootScope,$timeout,APP_CONSTANT) {
	var oiService = {};
	
	oiService.listOfOis =  function(sellid,callback,callbackError) {
			
				console.log(' Seller ID -->'+sellid);
		
				$http.get(APP_CONSTANT.REMOTE_HOST +'/seller/'+sellid)
				// On Success of $http call
				.success(function(data, status, headers, config) {
//					 $rootScope.globals = {
// 		 					Seller: {
// 		 					id:sellid 		 					
// 		 				 }
//					 };
					callback(data,sellid);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackError(data, headers);
				});
			};
			
		
		
			

		
		
		oiService.mark = function(oiid,callbackmark,callbackerrormark) {
			
				console.log('oiID -->'+oiid);
				

				
				 
				$http.post(APP_CONSTANT.REMOTE_HOST + '/seller/markshipped/'+oiid)
				// On Success of $http call
					
				
				.success(function(data, status, headers, config) {
					callbackmark(data, headers);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackerrormark(data, headers);
				});
		

		};
		
	
		return oiService;

})
