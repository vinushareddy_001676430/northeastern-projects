/**
 * 
 */

var omodule = angular.module("orderhistory.module");

omodule.controller('oController', function($scope,$rootScope,$location,$uibModal,oService) {
	var oCtrl = this;
	

	$scope.init = function () {
		 console.log($rootScope.globals.userSession.id);
		 oService.orderlist($rootScope.globals.userSession.id,callback,callbackDashboardListError);
	
	};
	
	$scope.back=function(){
		$location.path("/")
	}

	//doesnt need headers
	var callback = function(data) 
	{ // Status Code:200
		
		$scope.orders = data;	
		console.log($scope.orders);
		
    };
    
    var callbackDashboardListError = function(data,headers) {
    		$scope.message = data.message;
    		$scope.error = true;   
    };
	

});


omodule.service('oService', function($http,$rootScope,$timeout,APP_CONSTANT) {
	var oService = {};
	
	oService.orderlist =  function(custid,callback,callbackError) {
			
				console.log(' Customer ID -->'+custid);
		
				$http.get(APP_CONSTANT.REMOTE_HOST +'/displayOrdHistory/'+custid)
				// On Success of $http call
				.success(function(data, status, headers, config) {

					callback(data);
				}).error(function(data, status, headers, config) {					
					callbackError(data, headers);
				});
			};
			
		
	
	
		return oService;

})
