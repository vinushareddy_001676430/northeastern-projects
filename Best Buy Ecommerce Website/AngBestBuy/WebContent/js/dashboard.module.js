/**
 * 
 */

var dashboardModule = angular.module("dashboard.module");

dashboardModule.controller('dashboardController', function($scope,$rootScope,$location,$uibModal,dashboardService) {
	var dashCtrl = this;
	$scope.messageDash = "This is Dashboard";

	$scope.init = function () {
		
		dashboardService.listOfProducts(callback,callbackDashboardListError);
	//	dashboardService.getCustId($rootScope.globals.userSession.name,callbackSucess,callbackError);
	};

	dashCtrl.variables={	
			quantity: ''
	};	
	
	$scope.displayPopularProducts=function(){
		$location.path('/view/popularproducts');
	}
	
	$scope.addToCart = function (prodObj) {		
		
		console.log($rootScope.globals.userSession);
		if($rootScope.globals.userSession!=null){
			
		
		console.log(prodObj.rowId);
		console.log($rootScope.globals.userSession.id);
		console.log(dashCtrl.variables);
		dashboardService.addToCart($rootScope.globals.userSession.id,prodObj.rowId.productId,dashCtrl.variables,callback1,callbackerror1);
		}
		
		else{
			
			dashCtrl.openComponentModal('Please Login to Add to Cart');
			
		}
		

	};
	
	$scope.viewCart = function () {		


		$location.path('/cart/view');

	};
	
	$scope.ViewOrderHistory = function () {		


		$location.path('/orders/view');

	};
	//doesnt need headers
	var callback = function(data,headers) 
	{ // Status Code:200
		
		$scope.products = data;
		console.log($scope.products);
		
    };
    
    var callbackDashboardListError = function(data,headers) {
    		$scope.message = data.message;
    		$scope.error = true;   
    };
	var callback1 = function(data,headers) 
	{ // Status Code:200
		//$scope.products = data;
		dashCtrl.openComponentModal('Added to Cart Successfully');
		console.log("sucesss");
    };
    
    var callbackerror1 = function(data,headers) {
    	$scope.message = data.message;
    	$scope.error = true;   
    	console.log("error"+$scope.message);
    };
    
	var callbackViewCart = function(data,headers) 
	{
		
		$location.path('/cart/view');
	
    };
    dashCtrl.openComponentModal = function(msgToDisplay) {
		var modalInstance = $uibModal.open({
			animation : true,
			component : 'successComponent',
			resolve : {
				msg : function() {
					return msgToDisplay;
				}
			}
		});

	};
});


dashboardModule.service('dashboardService', function($http,$timeout,APP_CONSTANT) {
	var dashboardService = {};
	
	dashboardService.listOfProducts =  function(callback,callbackError) {
			if (APP_CONSTANT.DEMO) {
				console.log('ID -->'+id);
				/*
				 * Dummy authentication for testing, uses $timeout to simulate api
				 * call ----------------------------------------------
				 */
				$timeout(function() {

					var response;
					
					response = [
										{resumeId:12,name:"Coming to US",desc:"-",dateOfCreation:'20/Dec/2016'},
										{resumeId:23,name:"After Semester 1",desc:"testing app",dateOfCreation:'18/May/2016'},
										{resumeId:43,name:"For Co-op",desc:"",dateOfCreation:'1/Nov/2016'}
								];
					

					callback(response);
				}, 1000);
			} else {

				/*
				 * Use this for real authentication
				 * ----------------------------------------------
				 */
				$http.get(APP_CONSTANT.REMOTE_HOST +'/displayProducts')
				// On Success of $http call
				.success(function(data, status, headers, config) {
					callback(data);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackError(data, headers);
				});
			}

		};
		
				

		
		
		dashboardService.addToCart =  function(id,productid,data,callback1,callbackerror1) {
			if (APP_CONSTANT.DEMO) {
				console.log('ID -->'+id);
				
				$timeout(function() {

					var response;
					
					response = [
										{resumeId:12,name:"Coming to US",desc:"-",dateOfCreation:'20/Dec/2016'},
										{resumeId:23,name:"After Semester 1",desc:"testing app",dateOfCreation:'18/May/2016'},
										{resumeId:43,name:"For Co-op",desc:"",dateOfCreation:'1/Nov/2016'}
								];
					

					callback(response);
				}, 1000);
			} else {

				
				 
				$http.post(APP_CONSTANT.REMOTE_HOST + '/Customer/'+id+'/Product/'+productid+'/addOrderItem',data//+resumeid)
				// On Success of $http call
				
				)
				.success(function(data, status, headers, config) {
					callback1(data, headers);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackerror1(data, headers);
				});
			}

		};
		
	
	
	return dashboardService;
})
