/**
 * 
 */

var configModule = angular.module('app') // Please dont not use [], dependency 
.config(function($routeProvider) {	
//    $urlRouterProvider.otherwise('/login');
	$routeProvider
    // route for the home page
	.when('/', {
		 templateUrl : 'partial/home.html',
	     controller  : 'dashboardController'
	    
	})
	.when('/add/prod',{
		 templateUrl : 'partial/addProducts.html',
	     controller  : 'prodController',
	     controllerAs: 'prodCtrl'
		
	})
	.when('/login', {
		 templateUrl : 'partial/login.html',
	     controller  : 'authController',
	     controllerAs: 'authCtrl'
	})
	
		.when('/orders/view', {
		 templateUrl : 'partial/orderHistory.html',
	     controller  : 'oController',
	     controllerAs: 'oCtrl'
	})
	.when('/admin/dashboard', {
		 templateUrl : 'partial/displaySellers.html',
	     controller  : 'adminController',
	     controllerAs: 'adminCtrl'
	})
	.when('/view/popularproducts', {
		 templateUrl : 'partial/displayPopularProducts.html',
	     controller  : 'ppController'
	    
	})
	.when('/registration', {
		 templateUrl : 'partial/registration.html',
	     controller  : 'registrationController',
	     controllerAs: 'regCtrl'
	})
	.when('/registration/seller', {
		 templateUrl : 'partial/registrationseller.html',
	     controller  : 'sellerregistrationController',
	     controllerAs: 'sellregCtrl'
	})
	.when('/logout', {
		redirectTo: '/'
	})
   .when('/dashboard', {
        templateUrl : 'partial/home.html',
        controller  : 'dashboardController',
        controllerAs: 'dashCtrl'
    })
     .when('/view/oi', {
        templateUrl : 'partial/viewOrders.html',
        controller  : 'oiController',
        controllerAs: 'oiCtrl'
    })
    .when('/resume/add', {
        templateUrl : 'partial/resume-add.html',
        controller  : 'addResumeController'
    })
    .when('/cart/view', {
        templateUrl : 'partial/viewCart.html',
        controller  : 'cartController',
        controllerAs: 'cartCtrl'
    })
    .when('/skill', {
        templateUrl : 'partial/skill.html',
        controller  : 'skillContoller',
        controllerAs: 'skillCtrl'
    })
    .when('/personal', {
        templateUrl : 'partial/personal.html',
        controller  : 'personalContoller',
        controllerAs: 'personalCtrl'
    })
    .otherwise({ redirectTo: '/' });
})


.run(
    function ($rootScope, $location, $cookieStore,$window, $http,AUTH_EVENTS,APP_CONSTANT) {
    	//Management 
    	$rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
    		console.log('Clicked on '+$location.path());
            if ( !($location.path() == '/'
            		|| $location.path() == '/registration'
            		|| $location.path() == '/login'
            		||$location.path() == '/registration/seller'
            			||$location.path() == '/view/oi'
            				||$location.path() == '/add/prod'
            					||$location.path() == '/dashboard')
            		  
            		  && !$rootScope.globals.userSession) {
            		console.log('Invalid Path')
            		$location.path('/');
            }else if($location.path() == '/logout'){
            		$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            }else{
            
        		console.log('Valid Path')

            }
        });
    	
	$rootScope.$on(AUTH_EVENTS.loginFailed, function(event, next){
    		console.log('Login failed');
        

    		//$scope.message = "Login failed";
    });
	
	$rootScope.$on(AUTH_EVENTS.logoutSuccess, function(event, next){
		console.log('Logout Success');
		$window.localStorage.removeItem("globals");
		$rootScope.userSession=null;
	    $rootScope.$emit("CallParentMethod", {});

		//$scope.message = "Login failed";
});
    
    $rootScope.$on(AUTH_EVENTS.loginSuccess, function(event, next){
		//$scope.message = "Login Success";
		console.log('Login success');
	    $window.localStorage.setItem("globals", angular.toJson($rootScope.globals));
	    $rootScope.userSession = JSON.parse($window.localStorage.getItem("globals")).userSession;
	    
	    $rootScope.$emit("CallParentMethod", {});
	    
	    if($rootScope.userSession.role=="seller"){
	    	$location.path('/view/oi');
		    }
	    
    if($rootScope.userSession.role=="customer"){
    	$location.path('/dashboard');
	    }
    if($rootScope.userSession.role=="admin"){
    	$location.path('/admin/dashboard');
	    }
  
    
    });
    
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.userSession) {
        $http.defaults.headers.common[APP_CONSTANT.AUTH_KEY] = $rootScope.globals.userSession.authKey; // jshint ignore:line
	    $window.localStorage.setItem("globals", angular.toJson($rootScope.globals));
	    $rootScope.userSession = JSON.parse($window.localStorage.getItem("globals")).userSession;

    }

    

})



