/**
 * 
 */

var adminmod = angular.module("admin.module");

adminmod.controller('adminController', function($scope,$rootScope,$location,$uibModal,adminService) {
	
	var adminCtrl = this;
	

	$scope.init = function () {
		
		adminService.listOfSellers(callback,callbackDashboardListError);
	
	};
	
	$scope.reg=function(){
		$location.path('/registration/seller');
	}

//	dashCtrl.variables={	
//			quantity: ''
//	};			
	
	$scope.enable = function (sellerObj) {		
				
		console.log(sellerObj.rowId);
		//document.getElementById("enable").disabled =true;		
		adminService.enable(sellerObj.rowId.sellerId,callbackenable,callbackerrorenable);
		
	

	};
	
	$scope.disable = function (sellerObj) {		
		
		console.log(sellerObj.rowId);		
	//	document.getElementById("disable").disabled =true;	
		adminService.disable(sellerObj.rowId.sellerId,callbackdisable,callbackerrordisable);


	};
	
	var callback = function(data) 
	{ 
		
		$scope.sellers = data;
		console.log($scope.sellers);
		
    };
    
    var callbackDashboardListError = function(data) {
    		$scope.message = data.message;
    		$scope.error = true;   
    };
	var callbackenable = function(data) 
	{ 
		adminCtrl.openComponentModal('Enabled Sucessfully');
		console.log("sucesss");
    };
    
    var callbackerrorenable = function(data) {
    	$scope.message = data.message;
    	$scope.error = true;   
    	console.log("error"+$scope.message);
    };
    
	var callbackdisable = function(data,headers) 
	{
		adminCtrl.openComponentModal('Disabled Sucessfully');
		console.log("sucesss");
		
	
    };
    var callbackerrordisable = function(data,headers) 
	{
    $scope.message = data.message;
	$scope.error = true;   
	console.log("error"+$scope.message);
	};
	
	adminCtrl.openComponentModal = function(msgToDisplay) {
		var modalInstance = $uibModal.open({
			animation : true,
			component : 'successComponent',
			resolve : {
				msg : function() {
					return msgToDisplay;
				}
			}
		});

	};
});


adminmod.service('adminService', function($http,$timeout,APP_CONSTANT) {
	var adminService = {};
	
	adminService.listOfSellers=  function(callback,callbackError) {
			
				$http.get(APP_CONSTANT.REMOTE_HOST +'/displaysellers')
				// On Success of $http call
				.success(function(data, status, headers, config) {
					callback(data);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackError(data, headers);
				});
			};

		
		
				

		
		
			adminService.enable =  function(sellerid,callback1,callbackerror1) {
			
				console.log('Seller ID -->'+sellerid);
				
			
				$http.post(APP_CONSTANT.REMOTE_HOST + '/Admin/enable/'+sellerid,
						
						{
					status:"Enable"
						}
						
				
				// On Success of $http call
				
				)
				.success(function(data, status, headers, config) {
					callback1(data, headers);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackerror1(data, headers);
				});
			

		};
		
		adminService.disable =  function(sellerid,callback1,callbackerror1) {
			
			console.log('Seller ID -->'+sellerid);
			
		
			$http.post(APP_CONSTANT.REMOTE_HOST + '/Admin/disable/'+sellerid,
					
					{
				status:"Disable"
					}
					
			
			// On Success of $http call
			
			)
			.success(function(data, status, headers, config) {
				callback1(data, headers);
			}).error(function(data, status, headers, config) { // IF
				// STATUS
				// CODE
				// NOT
				// 200
				callbackerror1(data, headers);
			});
		

	};
	
	

	return adminService;
	
})
