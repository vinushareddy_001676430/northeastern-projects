/**
 * 
 */
'use strict';
// Step 1: declare modules
 angular.module("authModule",[]);
 angular.module("base.module",[]);
 angular.module("cart.module",[]);
 angular.module("welcome.module",[]);
 angular.module("registration.module",[]);
 angular.module("seller.registration.module",[]); 
 angular.module("popularproducts.module",[]);
 angular.module("dashboard.module", []);
 angular.module("resume.module", []);
 angular.module("orderitem.module",[]);
 angular.module("addprod.module",[]);
 angular.module("admin.module",[]);
 angular.module("orderhistory.module",[]); 



 



 angular
 .module('appCoreModule', [
	 'ngRoute',
     'ngCookies',
     'ui.bootstrap'
 ]);
//Step 2: Register App
angular.module("app", 	
			['appCoreModule',
			'welcome.module',
			'registration.module',
			'authModule',
			'dashboard.module',
			'popularproducts.module',
			'base.module',
			'cart.module',
			'resume.module',
			'orderitem.module',
			'addprod.module',
			'admin.module',
			'seller.registration.module',
			'orderhistory.module',
			
		 ]);



