/**
 * 
 */
var sellerregistrationModule = angular.module("seller.registration.module", []);
sellerregistrationModule.controller('sellerregistrationController', function($location,
		$scope, $uibModal, sellerregistrationService) {

	var sellregCtrl = this;

	sellregCtrl.registration = {
		title : "",
		firstName : "",
		lastName : "",
		username : "",
		password : "",
		email : "",
		
		phone : "",
		merchname:"",
		addline1: "",
		addline2: "",
		city: "",
		state: "",
		zip: ""		
		
	};
	
	$scope.reg=function(){
		$location.path('/registration/seller');
	}
	
//	$scope.validateLastName= function(){
//	var comments=document.getElementById("lastname").value;
//		var reg= /^[ a-zA-Z s ]*$/;
//		if(!(comments.match(reg))){
//			alert("Please Enter Only Alphabets");
//			return false;
//			}  
//			}
//	
	$scope.validateFirstName= function(){
	var comments=document.getElementById("firstname").value;
		var reg= /^[ a-zA-Z s ]*$/;
		if(!(comments.match(reg))){
			alert("Please Enter Only Alphabets");
			return false;
			}
	}
	
	$scope.validation=function (){
		if(firstname.value== "") {
			alert("Error: Enter All fields!");
		
		}
		   
		 else{alert("Submitted Successfully");	 

}
}



	sellregCtrl.dobOptions = {
		maxDate : new Date(),
		popup : false,
		format : 'dd-MMMM-yyyy',
		altInputFormats : [ 'M!/d!/yyyy' ]
	};

	sellregCtrl.setUpDob = function() {
		var todayDate = new Date();
		sellregCtrl.dobOptions.maxDate = new Date(todayDate.getFullYear() - 18,
				todayDate.getMonth(), todayDate.getDate());
		sellregCtrl.registration.dateOfBirth = sellregCtrl.dobOptions.maxDate;
	};

	sellregCtrl.dateOptions = {
		dateDisabled : disabled,
		formatYear : 'yy',
		maxDate : sellregCtrl.dobOptions.maxDate,
		// minDate : new Date(),
		startingDay : 1
	};
	// calling today function
	sellregCtrl.setUpDob();

	// Disable weekend selection
	function disabled(data) {
		// var date = data.date, mode = data.mode;
		// return mode === 'day'
		// && (date.getDay() === 0 || date.getDay() === 6);
		return false;
	}

	sellregCtrl.open = function() {
		sellregCtrl.dobOptions.popup = true;
	};

	sellregCtrl.cancel = function() {
		$location.path('/');
	}

	sellregCtrl.register = function() {
		console.log(sellregCtrl.registration);
		sellerregistrationService.register(sellregCtrl.registration, callbackSuccess,
				callbackError);

	}
	var callbackSuccess = function(data, headers) { // Status
		// Code:200
		console.log("sucess");
		
			sellregCtrl.openComponentModal('Registration Successful');
			$location.path('/');

		 

	};

	var callbackError = function(data, headers) {
		sellregCtrl.message = data.message;
		sellregCtrl.error = true;

	};

	sellregCtrl.openComponentModal = function(msgToDisplay) {
		var modalInstance = $uibModal.open({
			animation : true,
			component : 'successComponent',
			resolve : {
				msg : function() {
					return msgToDisplay;
				}
			}
		});

	};

});

sellerregistrationModule.factory('sellerregistrationService', function($rootScope, $http,
		$timeout, $cookieStore, $window, APP_CONSTANT, AUTH_EVENTS) {
	var sellerregistrationService = {};

	sellerregistrationService.register = function(data, callback, callbackError) {
		
			$http.post(APP_CONSTANT.REMOTE_HOST + '/registration/seller',
					{
				
				
					title:data.title,
					firstName:data.firstName,
					lastName:data.lastName,
					username:data.username,
					password :data.password,
					email:data.email,
					
					phone:data.phone,
					merchandiseName:data.merchname,
					addressLine1:data.addline1,
					addressLine2:data.addline2,
					city:data.city,
					state:data.state,
					zip:data.zip		
					
					}

			)
			// On Success of $http call
			.success(function(data, status, headers, config) {
				callback(data, headers);
				//console.log(sucess);
			}).error(function(data, status, headers, config) { // IF
				// STATUS
				// CODE
				// NOT
				// 200
				callbackError(data, headers);
			});
		

	};
	
	return sellerregistrationService;

});
