/**
 * 
 */

var ppmodule = angular.module("popularproducts.module");

ppmodule.controller('ppController', function($scope,$rootScope,$location,$uibModal,ppService) {
	
	

	$scope.init = function () {		 
		ppService.listOfps(callback,callbackDashboardListError);
	
	};
	
	$scope.back = function() {
		$location.path("/dashboard");
	}


	var callback = function(data,headers) 
	{ // Status Code:200
		
		$scope.popproducts = data;
		
		console.log($scope.popproducts);
		
    };
    
    var callbackDashboardListError = function(data,headers) {
    		$scope.message = data.message;
    		$scope.error = true;   
    };
	
});


ppmodule.service('ppService', function($http,$rootScope,$timeout,APP_CONSTANT) {
	var ppService = {};
	
	ppService.listOfps =  function(callback,callbackError) {
			
			
		
				$http.get(APP_CONSTANT.REMOTE_HOST +'/displayPopularProducts')
				// On Success of $http call
				.success(function(data, status, headers, config) {
			
					callback(data,headers);
				}).error(function(data, status, headers, config) { // IF
					
					callbackError(data, headers);
				});
			};
			
		
	
	
		return ppService;

})
