/**
 * 
 */

var prodmodule = angular.module("addprod.module");

prodmodule.controller('prodController', function($scope,$rootScope,$location,$uibModal,prodService) {
	var prodCtrl = this;
	


	prodCtrl.product= {	
			name:'',
			description:'',
			price: '',
			weight:'',
			type:''
	};			
	
	$scope.back=function(){
		$location.path("/view/oi")
	}
	
	prodCtrl.addprod=function(){
		console.log($rootScope.globals.userSession.id);
		prodService.add($rootScope.globals.userSession.id,prodCtrl.product,prodcallback,callbackerror);
		
	};
	


	//doesnt need headers
	var prodcallback = function(data) 
	{ // Status Code:200
		
		$scope.ois = data;
		console.log($scope.ois);
		
    };
    
    var callbackerror = function(data,headers) {
    		$scope.message = data.message;
    		$scope.error = true;   
    };
	
    
	
    prodCtrl.openComponentModal = function(msgToDisplay) {
		var modalInstance = $uibModal.open({
			animation : true,
			component : 'successComponent',
			resolve : {
				msg : function() {
					return msgToDisplay;
				}
			}
		});

	};
});


prodmodule.service('prodService', function($http,$timeout,APP_CONSTANT) {
	var prodService = {};
	
	prodService.add =  function(sellid,data,prodcallback,callbackerror) {
			
				console.log(' Seller ID -->'+sellid);
		
				$http.post(APP_CONSTANT.REMOTE_HOST +'/seller/'+sellid+'/Product',
						{
					productName:data.name,
					description:data.description,
					price:data.price,
					weight:data.weight,
					type:data.type
						}
						
				)
				// On Success of $http call
				.success(function(data, status, headers, config) {
					prodcallback(data);
				}).error(function(data, status, headers, config) { // IF
					// STATUS
					// CODE
					// NOT
					// 200
					callbackerror(data, headers);
				});
			};
			
		
	
		return prodService;

})
