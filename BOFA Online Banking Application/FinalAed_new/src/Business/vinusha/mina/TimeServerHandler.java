package Business.vinusha.mina;



import Business.vinusha.db4o.SessionManager;

import Business.vinusha.util.RegisterAtServer;
import Business.vinusha.dbTables.RegisterSuspiousActivity;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;



public class TimeServerHandler extends IoHandlerAdapter
{
	SessionManager sessnManager = new SessionManager();
	
	@Override
	public void exceptionCaught( IoSession session, Throwable cause ) throws Exception
	{
		cause.printStackTrace();
	}
	@SuppressWarnings("deprecation")
	@Override
	public void messageReceived( IoSession session, Object message ) throws Exception
	{   	
            //what message is coming in this method
		if(null!=message){	
			String str = message.toString();
			if( str.trim().equalsIgnoreCase("quit") ) {
				session.close();
				return;
			}

			if(!StringUtils.isEmpty(str)){  
				String rspObj	= new RegisterAtServer().processRequest(str,sessnManager);
				if(null==rspObj){
					session.write("Invalid");
				}else{
				session.write(rspObj);
				}
				System.out.println("Message written...");

			}else{
				RegisterSuspiousActivity sps = new RegisterSuspiousActivity();
				sps.setDate(new Date());
				sps.setIsresolved(false);
				sessnManager.getDatabaseConnector().store(sps);
				System.out.println("Suspious Activity Found");
				//Send EMail
			}
		}
	}

	

	@Override
	public void sessionIdle( IoSession session, IdleStatus status ) throws Exception
	{
		System.out.println( "IDLE " + session.getIdleCount( status ));
	}
}