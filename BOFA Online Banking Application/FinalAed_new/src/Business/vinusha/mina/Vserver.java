package Business.vinusha.mina;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

public class Vserver {

//	 private static final int PORT = 9123;
//	 
//	 public static void main(String args[]){
//	IoAcceptor acceptor = new NioSocketAcceptor();
//	try {
//		acceptor.getFilterChain().addLast( "logger", new LoggingFilter() );
//        acceptor.getFilterChain().addLast( "codec", new ProtocolCodecFilter( new TextLineCodecFactory( Charset.forName( "UTF-8" ))));
//        acceptor.setHandler(  new TimeServerHandler() );
//       // acceptor.getSessionConfig().setReadBufferSize( 2048 );
//        acceptor.getSessionConfig().setIdleTime( IdleStatus.BOTH_IDLE, 10 );
//		acceptor.bind( new InetSocketAddress(PORT) );
//		
//	} catch (IOException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//	
//	 
//	 }
    private static final int PORT = 9123;
	 
//	 public static void main(String args[]){
         
       public void startServer() throws IOException{
             
	IoAcceptor acceptor = new NioSocketAcceptor();
        
        acceptor.getFilterChain().addLast( "logger", new LoggingFilter() );
        acceptor.getFilterChain().addLast( "codec", new ProtocolCodecFilter( new TextLineCodecFactory( Charset.forName( "UTF-8" ))));
        acceptor.setHandler(  new TimeServerHandler() );
        acceptor.getSessionConfig().setReadBufferSize(122024);
        //acceptor.getSessionConfig().setReadBufferSize( 22222048 );
       
        acceptor.getSessionConfig().setIdleTime( IdleStatus.BOTH_IDLE, 10 );
	acceptor.bind( new InetSocketAddress(PORT) );
	
        }
	
}
