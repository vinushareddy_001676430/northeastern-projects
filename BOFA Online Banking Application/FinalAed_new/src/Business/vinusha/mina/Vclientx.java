package Business.vinusha.mina;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Vclientx {

	//public static final String REQUEST_TYPE="VALIDATEACCOUNT";
	
	
        Socket clientSocket = null;
        DataOutputStream outToServer = null;
        
	public void connectclient(){
            try {
                clientSocket = new Socket("127.0.0.1", 9123);
                outToServer = new DataOutputStream(clientSocket.getOutputStream());
            } catch (IOException ex) {
                Logger.getLogger(Vclientx.class.getName()).log(Level.SEVERE, null, ex);
            }
           	
        }
	
        public void closeConnect(){
            try {
                if(null!=clientSocket){ //any which ways we will definitely have a not null clientsocket right?
                clientSocket.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(Vclientx.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public String senddata(String requeststr) throws IOException{
            
            String returnVal = null;
            outToServer.writeBytes(requeststr + '\n');
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		String fromserver = inFromServer.readLine();
		if(null!=fromserver){
			if(fromserver.equalsIgnoreCase("Invalid Request")){
				returnVal = "Invalid";
			}else{
				returnVal = fromserver;
			}
			
		}
                return returnVal;
        }
        
        
	/*public static void main(String argv[]) throws Exception {
		String fromserver;
		Socket clientSocket = new Socket("127.0.0.1", 9123);
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		String jsonString=null;
		

		if(REQUEST_TYPE.equals("RegisterClient")){
			
			jsonString = new RegisterUser().registerUser("ram", "gopal","ramgopalc",REGISTER_BANKMANAGER);
			System.out.println(jsonString);
		}

		if(REQUEST_TYPE.equals("VALIDATEACCOUNT")){
			
			jsonString = new ValidateUser().validateUser("admin", "admin","VALIDATEACCOUNT","admin");
		}
		 
		
		
	}*/
}
