package Business.vinusha.util;

import Business.vinusha.db4o.SessionManager;
import com.db4o.ObjectSet;
import Business.vinusha.dbTables.UserAccount;


public class RetrieveReviwers {

	
	public String retrieveUser(String username,String password,String role,SessionManager sessnManager){
		
		String returnVal=null;
		
		UserAccount acnt = new UserAccount();
		
		acnt.setUsername(username);
		acnt.setPassword(password);
		acnt.setRole(role);
		
		ObjectSet result =  sessnManager.getDatabaseConnector().queryByExample(acnt);
	
		if(!result.isEmpty()){
			UserAccount found = (UserAccount)result.next();
			returnVal = found.getToken();
		}
	
		return returnVal;
	}
	
}
