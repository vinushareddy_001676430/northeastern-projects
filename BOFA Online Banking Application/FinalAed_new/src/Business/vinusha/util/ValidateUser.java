package com.vinusha.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.Gson;

public class ValidateUser {

	
	public static final String USER_NAME_KEY = "username";
	public static final String USER_PASSWORD_KEY = "password";
	public static final String REQUES_TYPE = "requestTypes";
	public static final String ROLE_TYPE = "role";
	
	public String validateUser(String username,String password, String registerkey,String role){
		
		String returnVal = null;
		Map<String,String> uMap = null;
                
		try{
		 uMap = new LinkedHashMap<String,String>();
		
		uMap.put(USER_NAME_KEY, username);
		uMap.put(USER_PASSWORD_KEY,password);
		uMap.put(REQUES_TYPE, registerkey);
		uMap.put(ROLE_TYPE, role);
		
		Gson gObj = new Gson();
		
		returnVal = gObj.toJson(uMap);
		
		}finally{
			uMap.clear();
		}
		
		return returnVal;
		
	}
	
}
