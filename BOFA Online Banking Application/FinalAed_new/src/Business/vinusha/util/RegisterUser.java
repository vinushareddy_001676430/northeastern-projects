package Business.vinusha.util;



import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.Gson;

public class RegisterUser {

	
	public static final String USER_NAME_KEY = "username";
	public static final String USER_PASSWORD_KEY = "password";
	public static final String USER_LOGINNAME_KEY = "userloginname";
	public static final String REQUES_TYPE = "requestTypes";
	
	public String registerUser(String username,String password,String userloginname,String registerkey){
		
		String returnVal = null;
		Map<String,String> uMap = null;
                
		try{
		 uMap = new LinkedHashMap<String,String>();
		
		uMap.put(USER_NAME_KEY, username);
		uMap.put(USER_PASSWORD_KEY,password);
		uMap.put(USER_LOGINNAME_KEY,userloginname);
		uMap.put(REQUES_TYPE, registerkey);
		
		Gson gObj = new Gson();
		
		returnVal = gObj.toJson(uMap);
		
		}finally{
			uMap.clear();
		}
		
		return returnVal;
		
	}
	
}
