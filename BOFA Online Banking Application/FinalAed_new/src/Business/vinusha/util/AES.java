package com.vinusha.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
 static String encryptionKey = "0123456789abcdef";
 // static String encryptionKey = "vinusha";

 public static byte[] encrypt(String value) {
     byte[] encrypted = null;
     try {

         byte[] raw = encryptionKey.getBytes();
         Key skeySpec = new SecretKeySpec(raw, "AES");
         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
         byte[] iv = new byte[cipher.getBlockSize()];

         IvParameterSpec ivParams = new IvParameterSpec(iv);
         cipher.init(Cipher.ENCRYPT_MODE, skeySpec,ivParams);
         encrypted  = cipher.doFinal(value.getBytes());
         System.out.println("encrypted string:" + encrypted.length);

     } catch (Exception ex) {
         ex.printStackTrace();
     }
     return encrypted;
 }

 public static  byte[]  decrypt(byte[] encrypted) {
     byte[] original = null;
     Cipher cipher = null;
    try {
    	byte[] raw = encryptionKey.getBytes();
        Key key = new SecretKeySpec(raw, "AES");
        cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        //the block size (in bytes), or 0 if the underlying algorithm is not a block cipher
        byte[] ivByte = new byte[cipher.getBlockSize()];
        //This class specifies an initialization vector (IV). Examples which use
        //IVs are ciphers in feedback mode, e.g., DES in CBC mode and RSA ciphers with OAEP encoding operation.
        IvParameterSpec ivParamsSpec = new IvParameterSpec(ivByte);
        cipher.init(Cipher.DECRYPT_MODE, key, ivParamsSpec);
        original= cipher.doFinal(encrypted);
    } catch (Exception ex) {
        ex.printStackTrace();
    }
    return original;
}
}