package Business.vinusha.util;




import Business.vinusha.db4o.SessionManager;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.google.gson.Gson;
import Business.vinusha.dbTables.CheckingAccount;
import Business.vinusha.dbTables.Customer;
import Business.vinusha.dbTables.CreditCard;
import Business.vinusha.dbTables.EducationLoan;
import Business.vinusha.dbTables.LoanOfficer;
import Business.vinusha.dbTables.RegisterSuspiousActivity;
import Business.vinusha.dbTables.SavingsAccount;
//import Business.vinusha.dbTables.SystemAdmin;
import Business.vinusha.dbTables.VechileLoan;
import com.vinusha.util.SessionIdentifierGenerator;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;





public class RegisterAtServer {

	Random rnd = new Random();

	private static final String ADMIN = "admin";
	private static final String CLIENT = "client";
	private static final String LOANMANAGER = "loanmanager";
	private static final String BANKMANAGER = "bankmanager";
	
	private static final String REGISTER_CLIENT = "REGISTERCLIENT";
	private static final String REGISTER_SYSADMIN = "REGISTERADMIN";
	private static final String REGISTER_LOANMANAGER = "REGISTERLMANAGER";
	private static final String REGISTER_BANKMANAGER = "REGISTERBMANAGER";
	private static final String VALIDATE_ACCOUNT = "VALIDATEACCOUNT";
	private static final String OPEN_ACCOUNT = "openaccount";
	
                public String processRequest(String str, SessionManager sessnManager) {

		String returnVal = null;
			if(JSONUtils.isJSONStringParsable(str)){

				Gson gobj = new Gson();
				@SuppressWarnings("unchecked")
				Map<String,String> xmap= gobj.fromJson(str,Map.class);
//				if(xmap.get("requestTypes").equalsIgnoreCase(REGISTER_SYSADMIN)){
//					SystemAdmin sa = new SystemAdmin();
//					sa.setUsername(xmap.get("username"));
//					sa.setPassword(xmap.get("password"));
//					
//					if(!validateUser(sa,sessnManager)){
//					sa.setUserloginname(xmap.get("userloginname"));	
//					sa.setToken(new SessionIdentifierGenerator().nextSessionId());
//					sa.setRole(ADMIN);
//					sessnManager.getDatabaseConnector().store(sa);
//					sessnManager.getDatabaseConnector().commit();
//					returnVal = "Registration Successfull";
//					}else{
//						returnVal = "User Exists";
//					}
//				}
                         if(xmap.get("requestTypes").equalsIgnoreCase(REGISTER_CLIENT)){
					Customer cd = new Customer();
					cd.setUsername(xmap.get("username"));
					cd.setPassword(xmap.get("password"));
					if(!validateUser(cd,sessnManager)){
					cd.setFirstname(xmap.get("firstname"));
					cd.setLastName(xmap.get("lastname"));
					cd.setEmailaddress(xmap.get("emailaddress"));
					cd.setPhonenumber(xmap.get("phonenumber"));
					cd.setCity(xmap.get("city"));
					cd.setState(xmap.get("state"));
					cd.setCountry(xmap.get("country"));
					cd.setDOB(xmap.get("dob"));
					cd.setZipcode(xmap.get("zipcode"));
					cd.setCountryresidence(xmap.get("countryresidence"));
					cd.setAddress(xmap.get("address"));
					cd.setUserloginname(xmap.get("userloginname"));	
					cd.setToken(new SessionIdentifierGenerator().nextSessionId());
					cd.setRole(CLIENT);
					cd.setIpaddress(xmap.get("ipaddress"));
					Random r = new Random( System.currentTimeMillis() );
					cd.setRefno(((1 + r.nextInt(2)) * 10000 + r.nextInt(10000)));
					sessnManager.getDatabaseConnector().store(cd);
					sessnManager.getDatabaseConnector().commit();
					returnVal = "Registration Successfull";
					}else{
						returnVal = "User Exists";	
					}
				}else if(xmap.get("requestTypes").equalsIgnoreCase(REGISTER_LOANMANAGER)){
					LoanOfficer ld = new LoanOfficer();
					ld.setUsername(xmap.get("username"));
					ld.setPassword(xmap.get("pasword"));
					if(!validateUser(ld,sessnManager)){
					ld.setUserloginname(xmap.get("userloginname"));	
					ld.setToken(new SessionIdentifierGenerator().nextSessionId());
					ld.setRole(LOANMANAGER);
					sessnManager.getDatabaseConnector().store(ld);
					sessnManager.getDatabaseConnector().commit();
					returnVal = "Registration Successfull";
					}else{
						returnVal = "User Exists";	
					}
//				}else if(xmap.get("requestTypes").equalsIgnoreCase(REGISTER_BANKMANAGER)){
//					BankManager bm = new BankManager();
//					bm.setUsername(xmap.get("username"));
//					bm.setPassword(xmap.get("password"));
//					if(!validateUser(bm,sessnManager)){
//					bm.setUserloginname(xmap.get("userloginname"));	
//					bm.setToken(new SessionIdentifierGenerator().nextSessionId());
//					bm.setRole(BANKMANAGER);
//					ObjectContainer ob = sessnManager.getDatabaseConnector();
//					ob.store(bm);
//					ob.commit();
//					returnVal = "Registration Successfull";
//					}else{
//						returnVal = "User Exists";	
//					}
				}else if(xmap.get("requestTypes").equalsIgnoreCase(VALIDATE_ACCOUNT)){
					
					String tkn = new RetrieveReviwers().retrieveUser(xmap.get("username"), xmap.get("password"), xmap.get("role"), sessnManager);
					
					if(null!=tkn){
						try {
							//String ecryptkey = new String(AES.encrypt(tkn));
							returnVal = tkn;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						returnVal = "Invalid Details";
					}
				}else if(xmap.get("requestTypes").equalsIgnoreCase(OPEN_ACCOUNT)){
						
						String token = xmap.get("token");
						
						if(null!=token){
							//String decryptedkey = new String(AES.decrypt(token.getBytes()));
							Customer cdtails = new Customer();
							cdtails.setToken(token);
							
							ObjectSet result =  sessnManager.getDatabaseConnector().queryByExample(cdtails);
							
							if(!result.isEmpty()){
								Customer found = (Customer)result.next();
								int refNo = found.getRefno();
								
								if(xmap.get("accounttype").equalsIgnoreCase("checkingaccount"))
								{
								CheckingAccount caount = new CheckingAccount();
								caount.setRefno(refNo);
								caount.setBalance(Integer.parseInt(xmap.get("balance")));
								caount.setStatus("draft");
								caount.setAccountno(new SessionIdentifierGenerator().nextSessionId());
								ObjectContainer ob = sessnManager.getDatabaseConnector();
								ob.store(caount);
								ob.commit();
								returnVal = "Account Created";
								}else if (xmap.get("accounttype").equalsIgnoreCase("savingaccount")){
									
									SavingsAccount sa = new SavingsAccount();
									sa.setRefno(refNo);
									sa.setBalance(Integer.parseInt(xmap.get("balance")));
									sa.setStatus("draft");
									sa.setAccountno(new SessionIdentifierGenerator().nextSessionId());
									ObjectContainer ob = sessnManager.getDatabaseConnector();
									ob.store(sa);
									ob.commit();
									returnVal = "Account Created";	
								}else if(xmap.get("accounttype").equalsIgnoreCase("creditcard")){
									
									CreditCard sa = new CreditCard();
									sa.setRefno(refNo);
									sa.setLimit(Integer.parseInt(xmap.get("limit")));
									sa.setSalary(Integer.parseInt(xmap.get("salary")));
									sa.setAccountno(new SessionIdentifierGenerator().nextSessionId());
									ObjectContainer ob = sessnManager.getDatabaseConnector();
									ob.store(sa);
									ob.commit();
									returnVal = "Account Created";
									
								}else if(xmap.get("accounttype").equalsIgnoreCase("educationloan")){
									
									EducationLoan sa = new EducationLoan();
									sa.setRefno(refNo);
									sa.setLimit(Integer.parseInt(xmap.get("limit")));
									sa.setCollegename(xmap.get("collegename"));
									sa.setStateofcollege(xmap.get("collegestate"));
									sa.setStatus("draft");
									sa.setAccountno(new SessionIdentifierGenerator().nextSessionId());
									ObjectContainer ob = sessnManager.getDatabaseConnector();
									ob.store(sa);
									ob.commit();
									returnVal = "Account Created";
									
								}else if(xmap.get("accounttype").equalsIgnoreCase("vechileloan")){
									
									VechileLoan sa = new VechileLoan();
									sa.setRefno(refNo);
									sa.setLoanammount(Integer.parseInt(xmap.get("loanamount")));
									sa.setVinno(xmap.get("vinnumber"));
									sa.setEmipercentage(Double.parseDouble("3"));
									sa.setStatus("draft");
									sa.setAccountno(new SessionIdentifierGenerator().nextSessionId());
									ObjectContainer ob = sessnManager.getDatabaseConnector();
									
									ob.store(sa);
									ob.commit();
									returnVal = "Account Created";
									
								}
							}else{
								returnVal = "Session timeout";
							}
						}else{
							returnVal = "Invalid Token";
						}
				}else if(xmap.get("requestTypes").equalsIgnoreCase("clientdetails")){
                                    
                                    
                                    String token = xmap.get("token");
                                    if(null!=token){
							//String decryptedkey = new String(AES.decrypt(token.getBytes()));
							Customer cdtails = new Customer();
							cdtails.setToken(token);
                                                        ObjectSet result =  sessnManager.getDatabaseConnector().queryByExample(cdtails);
							
							if(!result.isEmpty()){
								Customer found = (Customer)result.next();
								int refNo = found.getRefno();
                                                                
                                                                List<Map<String,String>> res = new ArrayList<Map<String,String>>();
                                                                CheckingAccount ck = new CheckingAccount();
                                                                ck.setRefno(refNo);
                                                                ObjectSet result1 =  sessnManager.getDatabaseConnector().queryByExample(ck);
                                                                Map<String,String> resmap = null;
                                                                if(!result1.isEmpty()){
                                                                  while(result1.hasNext()){
                                                                 CheckingAccount found1 = (CheckingAccount)result1.next();
                                                                    
                                                                        resmap = new LinkedHashMap<String,String>();
                                                                    
                                                                    resmap.put("accountno", found1.getAccountno());
                                                                    resmap.put("accounttype",found1.getAccounttype());
                                                                     resmap.put("balance",Integer.toString(found1.getBalance()));
                                                                    res.add(resmap);
                                                                    }
                                                                }
								
                                                                
                                                                
                                                                Gson gObj = new Gson();
		
                                                        returnVal = gObj.toJson(res);
                                                        
                                                        
                                                        
		
                                                                
                                                        }else{
                                                            returnVal="Invalid";
                                                        }
                                                        
                                    }else{
                                         returnVal="Invalid";
                                    }		
                                }
                                
			else{
				returnVal = "Invalid Request";
				RegisterSuspiousActivity sps = new RegisterSuspiousActivity();
				sps.setDate(new Date());
				sps.setIsresolved(false);
				ObjectContainer ob = sessnManager.getDatabaseConnector();
				ob.store(sps);
				ob.commit();
				System.out.println("Suspious Activity Found");
				//Send EMail
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
			}
		}else{
				returnVal = "Invalid Request";
				RegisterSuspiousActivity sps = new RegisterSuspiousActivity();
				sps.setDate(new Date());
				sps.setIsresolved(false);
				ObjectContainer ob = sessnManager.getDatabaseConnector();
				ob.store(sps);
				ob.commit();
				
				System.out.println("Suspious Activity Found");
				//Send EMail
			}
			return returnVal;
		}
		
		boolean validateUser(Object sa, SessionManager sessnManager) {

			boolean trigger = false;
		
			ObjectSet result = sessnManager.getDatabaseConnector().queryByExample(sa);
			
			if(null!=result && !result.isEmpty()){
				trigger = true;
			}

			
			return trigger;
		}

	
	}
