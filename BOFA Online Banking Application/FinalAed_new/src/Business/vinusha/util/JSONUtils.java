package Business.vinusha.util;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.IOException;



public final class JSONUtils {

  public static boolean isJSONStringParsable(String jsonString) {
	    try {
	        org.codehaus.jackson.JsonParser parser = 
	          new ObjectMapper().getJsonFactory().createJsonParser(jsonString);
	        while(parser.nextToken() != null) {
	        }
	        return true;
	    } catch (JsonParseException e) {
	        return false;
	    } catch (IOException e) {
	        return false;
	    }
	}
}