package Business.vinusha.db4o;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

public class SessionManager {

    public static final String filepath = "docbase.db4o";

    ObjectContainer db = null;

    public ObjectContainer getDatabaseConnector() {

        if (null == db) {
            db = Db4oEmbedded.openFile(Db4oEmbedded
                    .newConfiguration(), filepath);
        }
        return db;
    }
}
