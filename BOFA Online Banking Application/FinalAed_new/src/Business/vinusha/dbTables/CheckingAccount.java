package Business.vinusha.dbTables;

public class CheckingAccount {

    private String accountno = null;
    private int balance;
    private int refno;
    private String status = null;
//        private String assignedtoManager;
    private String accounttype = "checkingaccount";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRefno() {
        return refno;
    }

    public void setRefno(int refno) {
        this.refno = refno;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAccounttype() {
        return accounttype;
    }

}
