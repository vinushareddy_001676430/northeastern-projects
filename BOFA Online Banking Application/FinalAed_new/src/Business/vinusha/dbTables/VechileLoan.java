package Business.vinusha.dbTables;

public class VechileLoan {

    private String accounttype = "vechileloan";
    private String accountno = null;
    private int refno;
    private String status = null;
    private int loanammount;
    private double emipercentage;
    private String vinno = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRefno() {
        return refno;
    }

    public void setRefno(int refno) {
        this.refno = refno;
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public int getLoanammount() {
        return loanammount;
    }

    public void setLoanammount(int loanammount) {
        this.loanammount = loanammount;
    }

    public double getEmipercentage() {
        return emipercentage;
    }

    public void setEmipercentage(double emipercentage) {
        this.emipercentage = emipercentage;
    }

    public String getVinno() {
        return vinno;
    }

    public void setVinno(String vinno) {
        this.vinno = vinno;
    }

}
