package Business.vinusha.dbTables;

public class EducationLoan {

    private String accounttype = "educationloan";
    private String accountno = null;
    private int refno;
    private int limit;
    private String status = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRefno() {
        return refno;
    }

    public void setRefno(int refno) {
        this.refno = refno;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getCollegename() {
        return collegename;
    }

    public void setCollegename(String collegename) {
        this.collegename = collegename;
    }

    public String getStateofcollege() {
        return stateofcollege;
    }

    public void setStateofcollege(String stateofcollege) {
        this.stateofcollege = stateofcollege;
    }
    private String collegename = null;
    private String stateofcollege = null;

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

}
